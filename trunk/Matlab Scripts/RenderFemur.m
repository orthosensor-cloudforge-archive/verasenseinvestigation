%%
%
%
function [ output_args ] = RenderFemur( T )

    global f1LinkHandle f1_linkX_0 f1_linkY_0 f1_linkZ_0
    global f2LinkHandle f2_linkX_0 f2_linkY_0 f2_linkZ_0
    global f3LinkHandle j_x_0 j_y_0 j_z_0
    global f4LinkHandle hip_ball_jointX_0 hip_ball_jointY_0 hip_ball_jointZ_0
    
    if (0==nargin)
        set(f1LinkHandle,'XData',[],'YData',[],'ZData',[]);
        set(f2LinkHandle,'XData',[],'YData',[],'ZData',[]);
        set(f3LinkHandle,'XData',[],'YData',[],'ZData',[]);
        set(f4LinkHandle,'XData',[],'YData',[],'ZData',[]);
    else        
        [x,y,z]=tmultp3D(T,f1_linkX_0,f1_linkY_0,f1_linkZ_0);                       set(f1LinkHandle,'XData',x,'YData',y,'ZData',z);
        [x,y,z]=tmultp3D(T,f2_linkX_0,f2_linkY_0,f2_linkZ_0);                       set(f2LinkHandle,'XData',x,'YData',y,'ZData',z);
        [x,y,z]=tmultp3D(T,j_x_0,j_y_0,j_z_0);                                      set(f3LinkHandle,'XData',x,'YData',y,'ZData',z);
        [x,y,z]=tmultp3D(T,hip_ball_jointX_0,hip_ball_jointY_0,hip_ball_jointZ_0);  set(f4LinkHandle,'XData',x,'YData',y,'ZData',z);
    end
end

