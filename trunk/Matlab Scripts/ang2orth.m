%
%   Calculates the rotation matrix given angles in radians.
%
function orthm = ang2orth(ang) 

sa = sin(ang(2)); ca = cos(ang(2)); 
sb = sin(ang(1)); cb = cos(ang(1)); 
sc = sin(ang(3)); cc = cos(ang(3)); 

ra = [  ca,  sa,  0; ... 
       -sa,  ca,  0; ... 
         0,   0,  1]; 
rb = [  cb,  0,  sb; ... 
         0,  1,  0; ... 
       -sb,  0,  cb]; 
rc = [  1,   0,   0; ... 
        0,   cc, sc;... 
        0,  -sc, cc]; 
orthm = rc*rb*ra; 
end


function ang = orth2ang(orthm) 
ang(1) = asin(orthm(1,3)); 
ang(2) = angle( orthm(1,1:2)*[1 ;i] ); 
yz = orthm* ... 
    [orthm(1,:)',... 
     [-sin(ang(2)); cos(ang(2)); 0],... 
     [-sin(ang(1))*cos(ang(2)); -sin(ang(1)*sin(ang(2))); 
cos(ang(1))] ]; 

ang(3) = angle(yz(2,2:3)* [1; i]);
    
end

