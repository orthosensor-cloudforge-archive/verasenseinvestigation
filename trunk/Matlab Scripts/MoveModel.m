%%  
%   This script drives the model with known angles.
%   
%   Joint variables are theta_0 and theta_1. (rotations of hip and ball joint)
%

if (0==exist('femur_length','var'))
    hipJointModel;  % Load geometry.
else
    %   We already know the geometry.  Notice this requires manually rerunning hipJointModel if the model changes (which it
    %   shouldn't).
end

if ~exist('moveModel_switch','var')
    moveModel_switch=2; % Default movement.
end
    
switch(1)
    case 1
        %%
        %   Swing the model up/down left/right.
        %
        degrees=0;
        theta_1=0;  % Default angle.
        theta_2=0;  % Default angle.
        %for (degrees=0:5:360) 
        cpu_start=cputime;

        swing_direction=+1;
        for (theta_1=-30:10:+30)    % Rotate around X axis of frame 0.
            swing_direction=-1*swing_direction;
            for (theta_2=-30:2:+30)   % Rotate around Y axis of frame 0.            
                %%
                %   Find transformation for these joint variables.
                %
                JTable(1,4)=theta_1;
                JTable(2,4)=swing_direction*theta_2;
                %%
                %   Get the Transforms ready for the joint variables.
                %   These transform points from the ith frame to the zeroth.
                %
                T01=tofi(JTable(1,1),JTable(1,2),JTable(1,3),JTable(1,4));
                T02=tmultt(T01,tofi(JTable(2,:)));
                T03=tmultt(T02,tofi(JTable(3,:)));
                T04=tmultt(T03,tofi(JTable(4,:)));
                T05=tmultt(T04,tofi(JTable(5,:)));
                %%
                %   Transform all the model points.
                %   Update the plot information.
                %
                [x,y,z]=tmultp3D(T02,hip_ball_jointX_0,hip_ball_jointY_0,hip_ball_jointZ_0);    % Transform from ith to zero frame coordinates.
                set(hip_ball_handle,    'XData',x,'YData',y,'ZData',z);

                [x,y,z]=tmultp3D(T02,f1_linkX_0,f1_linkY_0,f1_linkZ_0);
                set(f1_link_handle,     'XData',x,'YData',y,'ZData',z);

                [x,y,z]=tmultp3D(T03,f2_linkX_0,f2_linkY_0,f2_linkZ_0);
                set(f2_link_handle,     'XData',x,'YData',y,'ZData',z);

                [x,y,z]=tmultp3D(T03,j_x_0,j_y_0,j_z_0);
                set(j_radius_handle,    'XData',x,'YData',y,'ZData',z);

                [x,y,z]=tmultp3D(T04,knee_jointX_0,knee_jointY_0,knee_jointZ_0);
                set(knee_joint_handle,  'XData',x,'YData',y,'ZData',z);

                [x,y,z]=tmultp3D(T05,razor_x_0,razor_y_0,razor_z_0);
                set(razor_handle,       'XData',x,'YData',y,'ZData',z);

                if (1)
                   units=units0;                   

                   quiver3(units(1,1),units(2,1),units(3,1),(units(1,2)-units(1,1)), (units(2,2)-units(2,1)), (units(3,2)-units(3,1)),'r')
                   quiver3(units(1,1),units(2,1),units(3,1),(units(1,3)-units(1,1)), (units(2,3)-units(2,1)), (units(3,3)-units(3,1)),'g')
                   quiver3(units(1,1),units(2,1),units(3,1),(units(1,4)-units(1,1)), (units(2,4)-units(2,1)), (units(3,4)-units(3,1)),'b')

                   units=tmultp(T03,units0);
                   quiver3(units(1,1),units(2,1),units(3,1),(units(1,2)-units(1,1)), (units(2,2)-units(2,1)), (units(3,2)-units(3,1)),'r')
                   quiver3(units(1,1),units(2,1),units(3,1),(units(1,3)-units(1,1)), (units(2,3)-units(2,1)), (units(3,3)-units(3,1)),'g')
                   quiver3(units(1,1),units(2,1),units(3,1),(units(1,4)-units(1,1)), (units(2,4)-units(2,1)), (units(3,4)-units(3,1)),'b')

                   units=tmultp(T04,units0);
                   quiver3(units(1,1),units(2,1),units(3,1),(units(1,2)-units(1,1)), (units(2,2)-units(2,1)), (units(3,2)-units(3,1)),'r')
                   quiver3(units(1,1),units(2,1),units(3,1),(units(1,3)-units(1,1)), (units(2,3)-units(2,1)), (units(3,3)-units(3,1)),'g')
                   quiver3(units(1,1),units(2,1),units(3,1),(units(1,4)-units(1,1)), (units(2,4)-units(2,1)), (units(3,4)-units(3,1)),'b')

                   units=tmultp(T05,units0);
                   quiver3(units(1,1),units(2,1),units(3,1),(units(1,2)-units(1,1)), (units(2,2)-units(2,1)), (units(3,2)-units(3,1)),'r')
                   quiver3(units(1,1),units(2,1),units(3,1),(units(1,3)-units(1,1)), (units(2,3)-units(2,1)), (units(3,3)-units(3,1)),'g')
                   quiver3(units(1,1),units(2,1),units(3,1),(units(1,4)-units(1,1)), (units(2,4)-units(2,1)), (units(3,4)-units(3,1)),'b')
                   
                   %set(quiver_handle_x(j)=quiver3(units(1,1),units(2,1),units(3,1), (units(1,2)-units(1,1)), (units(2,2)-units(2,1)), (units(3,2)-units(3,1)))
                   %set(quiver_handle_y(j)=quiver3(units(1,1),units(2,1),units(3,1), (units(1,3)-units(1,1)), (units(2,3)-units(2,1)), (units(3,3)-units(3,1)))
                   %set(quiver_handle_z(j)=quiver3(units(1,1),units(2,1),units(3,1), (units(1,4)-units(1,1)), (units(2,4)-units(2,1)), (units(3,4)-units(3,1)))
                end
                drawnow
            end
        end
        disp(sprintf('CPU time for loop is %4.2f.',cputime-cpu_start))
    case 2,
        %%
        %   Rotational circle.
        %
        for (d=0:2:360)
            %%
            %   Find transformation for these joint variables.
            %
            JTable(1,4)=30*cos(pi/180*d) - tan(f1_link_length/f2_link_length)*180/pi;   % theta_1.  
            JTable(2,4)=30*sin(pi/180*d);   % theta_2;            
            %%
            %   Get the Transforms ready for the joint variables.
            %   These transform points from the ith frame to the zeroth.
            %
            T01=tofi(JTable(1,1),JTable(1,2),JTable(1,3),JTable(1,4));
            T02=tmultt(T01,tofi(JTable(2,:)));
            T03=tmultt(T02,tofi(JTable(3,:)));
            T04=tmultt(T03,tofi(JTable(4,:)));
            T05=tmultt(T04,tofi(JTable(5,:)));
            %%
            %   Transform all the model points.
            %   Update the plot information.
            %
            [x,y,z]=tmultp3D(T02,hip_ball_jointX_0,hip_ball_jointY_0,hip_ball_jointZ_0);    % Transform from ith to zero frame coordinates.
            set(hip_ball_handle,    'XData',x,'YData',y,'ZData',z);

            [x,y,z]=tmultp3D(T02,f1_linkX_0,f1_linkY_0,f1_linkZ_0);
            set(f1_link_handle,     'XData',x,'YData',y,'ZData',z);

            [x,y,z]=tmultp3D(T03,f2_linkX_0,f2_linkY_0,f2_linkZ_0);
            set(f2_link_handle,     'XData',x,'YData',y,'ZData',z);

            [x,y,z]=tmultp3D(T03,j_x_0,j_y_0,j_z_0);
            set(j_radius_handle,    'XData',x,'YData',y,'ZData',z);

            [x,y,z]=tmultp3D(T04,knee_jointX_0,knee_jointY_0,knee_jointZ_0);
            set(knee_joint_handle,  'XData',x,'YData',y,'ZData',z);

            [x,y,z]=tmultp3D(T05,razor_x_0,razor_y_0,razor_z_0);
            set(razor_handle,       'XData',x,'YData',y,'ZData',z);

            if (0)
                [x,y,z]=tmultp(T01,units0);
                set(quiver_handle_x(j),'XData',x,'YData',y,'ZData',z);
    %           set(quiver_handle_x(j)=quiver3(units(1,1),units(2,1),units(3,1), (units(1,2)-units(1,1)), (units(2,2)-units(2,1)), (units(3,2)-units(3,1)))
    %           set(quiver_handle_y(j)=quiver3(units(1,1),units(2,1),units(3,1), (units(1,3)-units(1,1)), (units(2,3)-units(2,1)), (units(3,3)-units(3,1)))
    %           set(quiver_handle_z(j)=quiver3(units(1,1),units(2,1),units(3,1), (units(1,4)-units(1,1)), (units(2,4)-units(2,1)), (units(3,4)-units(3,1)))
            end
            drawnow
        end % for (degrees) loop.
    case 3
        %%
        %   Follow sensor orientation data. (Yaw, pitch)
        %
        for (i=1:size(YPR,1))
            %%
            %   Find transformation for these joint variables.
            %
            JTable(1,4)=YPR(i,1) - tan(f1_link_length/f2_link_length)*180/pi;   % theta_1.  
            JTable(2,4)=YPR(i,2);   
            %%
            %   Get the Transforms ready for the joint variables.
            %   These transform points from the ith frame to the zeroth.
            %
            T01=tofi(JTable(1,1),JTable(1,2),JTable(1,3),JTable(1,4));
            T02=tmultt(T01,tofi(JTable(2,:)));
            T03=tmultt(T02,tofi(JTable(3,:)));
            T04=tmultt(T03,tofi(JTable(4,:)));
            T05=tmultt(T04,tofi(JTable(5,:)));
            %%
            %   Transform all the model points.
            %   Update the plot information.
            %
            [x,y,z]=tmultp3D(T02,hip_ball_jointX_0,hip_ball_jointY_0,hip_ball_jointZ_0);    % Transform from ith to zero frame coordinates.
            set(hip_ball_handle,    'XData',x,'YData',y,'ZData',z);

            [x,y,z]=tmultp3D(T02,f1_linkX_0,f1_linkY_0,f1_linkZ_0);
            set(f1_link_handle,     'XData',x,'YData',y,'ZData',z);

            [x,y,z]=tmultp3D(T03,f2_linkX_0,f2_linkY_0,f2_linkZ_0);
            set(f2_link_handle,     'XData',x,'YData',y,'ZData',z);

            [x,y,z]=tmultp3D(T03,j_x_0,j_y_0,j_z_0);
            set(j_radius_handle,    'XData',x,'YData',y,'ZData',z);

            [x,y,z]=tmultp3D(T04,knee_jointX_0,knee_jointY_0,knee_jointZ_0);
            set(knee_joint_handle,  'XData',x,'YData',y,'ZData',z);

            [x,y,z]=tmultp3D(T05,razor_x_0,razor_y_0,razor_z_0);
            set(razor_handle,       'XData',x,'YData',y,'ZData',z);

            if (0)
                [x,y,z]=tmultp(T01,units0);
                set(quiver_handle_x(j),'XData',x,'YData',y,'ZData',z);
    %           set(quiver_handle_x(j)=quiver3(units(1,1),units(2,1),units(3,1), (units(1,2)-units(1,1)), (units(2,2)-units(2,1)), (units(3,2)-units(3,1)))
    %           set(quiver_handle_y(j)=quiver3(units(1,1),units(2,1),units(3,1), (units(1,3)-units(1,1)), (units(2,3)-units(2,1)), (units(3,3)-units(3,1)))
    %           set(quiver_handle_z(j)=quiver3(units(1,1),units(2,1),units(3,1), (units(1,4)-units(1,1)), (units(2,4)-units(2,1)), (units(3,4)-units(3,1)))
            end
            drawnow
            %pause(0.010);
        end
    case 4
        %%
        %   Move model to latest value of YPR available.
        %
        JTable(1,4)=YPR(i,1) - tan(f1_link_length/f2_link_length)*180/pi;   % theta_1.  
        JTable(2,4)=YPR(i,2);   
        %%
        %   Get the Transforms ready for the joint variables.
        %   These transform points from the ith frame to the zeroth.
        %
        T01=tofi(JTable(1,1),JTable(1,2),JTable(1,3),JTable(1,4));
        T02=tmultt(T01,tofi(JTable(2,:)));
        T03=tmultt(T02,tofi(JTable(3,:)));
        T04=tmultt(T03,tofi(JTable(4,:)));
        T05=tmultt(T04,tofi(JTable(5,:)));
        %%
        %   Transform all the model points.
        %   Update the plot information.
        %
        [x,y,z]=tmultp3D(T02,hip_ball_jointX_0,hip_ball_jointY_0,hip_ball_jointZ_0);    % Transform from ith to zero frame coordinates.
        set(hip_ball_handle,    'XData',x,'YData',y,'ZData',z);

        [x,y,z]=tmultp3D(T02,f1_linkX_0,f1_linkY_0,f1_linkZ_0);
        set(f1_link_handle,     'XData',x,'YData',y,'ZData',z);

        [x,y,z]=tmultp3D(T03,f2_linkX_0,f2_linkY_0,f2_linkZ_0);
        set(f2_link_handle,     'XData',x,'YData',y,'ZData',z);

        [x,y,z]=tmultp3D(T03,j_x_0,j_y_0,j_z_0);
        set(j_radius_handle,    'XData',x,'YData',y,'ZData',z);

        [x,y,z]=tmultp3D(T04,knee_jointX_0,knee_jointY_0,knee_jointZ_0);
        set(knee_joint_handle,  'XData',x,'YData',y,'ZData',z);

        [x,y,z]=tmultp3D(T05,razor_x_0,razor_y_0,razor_z_0);
        set(razor_handle,       'XData',x,'YData',y,'ZData',z);

        if (0)
            [x,y,z]=tmultp(T01,units0);
            set(quiver_handle_x(j),'XData',x,'YData',y,'ZData',z);
%           set(quiver_handle_x(j)=quiver3(units(1,1),units(2,1),units(3,1), (units(1,2)-units(1,1)), (units(2,2)-units(2,1)), (units(3,2)-units(3,1)))
%           set(quiver_handle_y(j)=quiver3(units(1,1),units(2,1),units(3,1), (units(1,3)-units(1,1)), (units(2,3)-units(2,1)), (units(3,3)-units(3,1)))
%           set(quiver_handle_z(j)=quiver3(units(1,1),units(2,1),units(3,1), (units(1,4)-units(1,1)), (units(2,4)-units(2,1)), (units(3,4)-units(3,1)))
        end
        drawnow
        %pause(0.010);
    otherwise
        disp('MoveModel.m Unknown switch.');
end % switch
if (0)
    %
    %   Draw final coordinates.
    %
    for (j=1:size(JTable,1))
        units=tmultp(t0i(j,JTable),units0);

        quiver_handle_x(j)=quiver3(units(1,1),units(2,1),units(3,1), (units(1,2)-units(1,1)), (units(2,2)-units(2,1)), (units(3,2)-units(3,1)),'r','LineWidth',1);
        quiver_handle_y(j)=quiver3(units(1,1),units(2,1),units(3,1), (units(1,3)-units(1,1)), (units(2,3)-units(2,1)), (units(3,3)-units(3,1)),'g','LineWidth',1);
        quiver_handle_z(j)=quiver3(units(1,1),units(2,1),units(3,1), (units(1,4)-units(1,1)), (units(2,4)-units(2,1)), (units(3,4)-units(3,1)),'b','LineWidth',1);
    end
end