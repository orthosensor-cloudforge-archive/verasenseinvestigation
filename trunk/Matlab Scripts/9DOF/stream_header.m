    %%
    %   "Gyro zero Calibrating (keep board still during this)." string from the Arduino.
    %
    disp('stream_header');
    
    pause(8.0);
    a=fread(s_port,23+30,'uchar');  
    if (~isempty(a))
        disp(char(a)')
        disp('');
        %
        %   "setup() done." message.
        %
        a=fread(s_port,16,'uchar');
        if (isempty(a))
            disp('Missing setup() done. string.');
            fclose(s_port);
            return;
        else
            disp(char(a)')
        end

        if (s_port.BytesAvailable >= (s_port.InputBufferSize-16))
            s_port.BytesAvailable
            disp('Waited too long (probably lost bytes).')
            fclose(s_port);
            return
        else
            fread(s_port,s_port.BytesAvailable,'uchar');    % Just dump these values and start fresh when this function returns.
        end
        
        %
        %   Sync up.
        %
        sync=0;
        while (0 == sync)
            header=fread(s_port,1,'uchar');
            while (header ~= 250)   % check for 0xFA.        
                disp(['Looking for header. 0x',dec2hex(header)]);
                header=fread(s_port,1,'uchar');
            end
            %
            %   Found the streaming command, check the length.
            %
            byte_count=fread(s_port,1,'uchar');
            if (20 == byte_count)
                sync=1;  % Found it.
                payload=fread(s_port,(byte_count/2),'int16')';  % Read the rest of the packet to stay in sync.
            else
                disp('Wrong length.')
            end
        end
    else
        disp('Missing - Gyro zero Calibrating (keep board still during this).');
        fclose(s_port);
        s_port=[];
    end   
    
    