%%
%   Test the whole suite of scripts
%
addpath([pwd,'\AHRS'],'-end');  % AHRS function scripts are in AHRS subdirectory.

if ~exist('switch_case','var')
    switch_case=3;
end
switch (switch_case)
    case 1,
        %%
        %   Use simulation information.
        %
        perfect_path   % Generates t_samples, accel, YPR for a perfect rotational path.
        position_integrated=test_position(t_samples,accel,YPR,velocity,position);
        gyro=[];
        magn=[];
        
        [T,Ts,norm(position_integrated(size(position_integrated,1),:)-position(1,:))]
    case 2,
        %%
        %   Collect new data.
        %
        test_stream;    % Collects the serial data from the aruduion and 9DOF board.
    case 3,
        %%o
        %   Use existing file of data.
        %
        load IMU_data_27-Oct-2011_15_46.mat; % Load the desired data file.
    case 4,
        %%
        %   Use whatever data is residing in the workspace. 
        %   Useful to run the last collected data set.
        %
    otherwise
        disp('Not a good switch case.');
        return;
end
%%
%   Plot the buffers.
%
clear ypr_arduino; % Disable plotting this variable. (if it exists)
stream_plot

if (1 == switch_case)
    %%
    %   Simulation values do not need to be trimmed.
    %
else
    %%
    %   Find the movement within the time period.  This is important as the integration will have increasing error over long time
    %   periods.
    %
    [index_first,index_last]=Movement_Detection(accel,gyro,magn,t_samples,0.05);
    %%
    %   Only keep the data over the relevant period.   
    %
    accel=accel(index_first:index_last,:);
    gyro=gyro(index_first:index_last,:);
    magn=magn(index_first:index_last,:);
    t_samples=t_samples(index_first:index_last,:);

    YPR=YPR(index_first:index_last,:);
    q_ML=q_ML(index_first:index_last,:);
    %%
    %   YPR is calculated in real time durinog data collection but this calculation will always have some delay.
    %   Running the data in reverse should remove this delay.
    %
    stream_plot
    YPR_fw=YPR; % This is the original estimate calculated in real time.
    q=q_ML(size(accel,1),:)';  % Initialize to last value as starting point for reverse.
    for (i=size(accel,1)-1:-1:1)
        %%
        %   Update AHRS algorithm. (with reverse time samples)
        %
        q=AHRSUpdate(accel(i,:)',gyro(i,:)',magn(i,:)',(t_samples(i+1)-t_samples(i))/2,q);                   
        YPR(i,:)=getYawPitchRoll(q); 
    end
    %
    %   Replot over just the period of interest.
    %
    stream_plot
    %%
    %   Integrate over time.
    %
    [position_integrated]=test_position(t_samples,accel,YPR,[0,0,0],[0,0,0]);
end

%
%   Move the model. (Only responds to pitch and yaw.)
%
moveModel_switch=3;
MoveModel      % Call script to animate the model.              

