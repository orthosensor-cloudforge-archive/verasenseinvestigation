%%
%   Test orientation correction.
o
%   Describe points (origin and 3 unit axis) to illustrate the coordinate axis. 
%   Then transform a set of unit axis for each frame.
%
figure(7), clf
units0=[0 0 0; 1 0 0; 0 1 0;  0 0 1]';  % Orthonormal axis of the IMU board.  Columns are 3D points.

%ypr=[0;0;0]    
%   [Yaw; Pitch; Roll] orientation.
ypr=[ 45;     10;   30];   

%
%   Transform coordinates from IMU back.
%   ypr contains Yaw, Pitch, Roll.
rotation_x=t1r1( ypr(3,1),0,0);     % Roll Rotation about x. 
rotation_y=t1r1(-ypr(2,1),0,1);     % Pitch rotation about y. Notice sign reversal to get relative to IMU rather than aircraft convention.
rotation_z=t1r1(-ypr(1,1),0,2);     % Yaw rotation about z.  Notice sign reversal to get relative to IMU rather than aircraft convention.
%
%   This matrix transforms the IMU coordinates.
%
r=rotation_z(:,1:3)*rotation_y(:,1:3)*rotation_x(:,1:3);    % Only need the rotation portion of the translation matrix.


units=r*units0;

quiver_handle_x=quiver3(units(1,1),units(2,1),units(3,1), (units(1,2)-units(1,1)), (units(2,2)-units(2,1)), (units(3,2)-units(3,1)),'r','LineWidth',1);
hold on
axis equal
axis([-1 +1 -1 +1 -1 +1]);
xlabel('x[m]')
ylabel('y[m]')
zlabel('z[m]')

quiver_handle_y=quiver3(units(1,1),units(2,1),units(3,1), (units(1,3)-units(1,1)), (units(2,3)-units(2,1)), (units(3,3)-units(3,1)),'g','LineWidth',1);
quiver_handle_z=quiver3(units(1,1),units(2,1),units(3,1), (units(1,4)-units(1,1)), (units(2,4)-units(2,1)), (units(3,4)-units(3,1)),'b','LineWidth',1);

if (0)
    x=get(quiver_handle_x,'XData')
    y=get(quiver_handle_x,'YData')
    z=get(quiver_handle_x,'ZData')
end