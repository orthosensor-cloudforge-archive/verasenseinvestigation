format compact
format short

%
%   Define the heading, pitch, roll.
%
yaw=0*pi/180;
pitch=2*pi/180;
roll= 1*pi/180;
%
%   See AN3192 eq5-10.  Note Figure 8 shows chip coordinates relative to aerospace convention. 
%
Ry=[[cos(yaw),sin(yaw),0];[-sin(yaw),cos(yaw),0];[0,0,1]]
Rp=[[cos(pitch),0,-sin(pitch)];[0,1,0];[sin(pitch),0,cos(pitch)]]
Rr=[[1,0,0];[0,cos(roll),sin(roll)];[0,-sin(roll),cos(roll)]]
%
%  Find the position relative to original frame when yaw, pitch, roll are applied.
%
accel=(Rr*Rp*Ry)*[0,0,1]'

mag=[1,0,0]';
CompassHeading(accel,mag);
mag=[+1,+1,0]'; mag=mag/norm(mag);
CompassHeading(accel,mag);
mag=[ 0,+1,0]';
CompassHeading(accel,mag);
mag=[+1,+1,0]'; mag=mag/norm(mag);
CompassHeading(accel,mag);
mag=[-1,0,0]';
CompassHeading(accel,mag);
mag=[+1,+1,0]'; mag=mag/norm(mag);
CompassHeading(accel,mag);
mag=[ 0,-1,0]';
CompassHeading(accel,mag);
mag=[+1,+1,0]'; mag=mag/norm(mag);
CompassHeading(accel,mag);

mag=[+1,-.001,0]'; mag=mag/norm(mag);
CompassHeading(accel,mag);
mag=[+1,+.001,0]'; mag=mag/norm(mag);
CompassHeading(accel,mag);
%
%   Test without normalized mag vector.
%
mag=[+1,+1,0]';
CompassHeading(accel,mag);

%
%   Test with extra linear accel vector.
%
accel=(Rr*Rp*Ry)*[0,0,1.1]'
CompassHeading(accel,mag);

if (0)
    for (i=0:360)
        phase=-i*pi/180;
        mag(1)=cos(phase);
        mag(2)=sin(phase);

        CompassHeading(accel,mag);
    end
end

    