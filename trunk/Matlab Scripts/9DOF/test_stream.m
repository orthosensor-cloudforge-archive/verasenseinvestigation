%%
%   This script reads serial stream from Aruduino 9DOF board and displays the values.
%
addpath([pwd,'\AHRS'],'-end');  % AHRS function scripts are in AHRS subdirectory.

if (nargin ~= 1)
    port=10; % Default to this com port.
end
    
%%
%   Constants used in this function.
%
Fupdate=140.5;          % Approx update rate in hz.  (needs to be calibrated or timestamped)
Twindow=1;              % Timespan in realtime plot window display.
STREAM_N=Fupdate*60;    % Number of steaming messages to catch before we quit.
byte_count=0;

Raircraft=[[-1,0,0];[0,+1,0];[0,0,+1]];  % Rotation matrix from sensor measurements to aircraft coordinates.

clear t_samples accel gyro magn YPR ypr_arduino % Clear variables from workspace.
AHRSInit                                        % Clears internal persistent variables.

disp(['Orthosense 9DOF test ',num2str(STREAM_N),' packets.']); 
%%
%   Load calibration values from previous runs.
%
if (2 == exist('accel_calibration.mat'))
    load accel_calibration.mat  % Retrieves accel_calibration matrix from calibrate_accel.m run.
    load compass_calibration.mat % Retrieves compass_calibration matrix from calibrate_compass.m run.
else
    disp('Calibration file does not exist.')
    accel_calibration=0.0038*eye(3,4);
    compass_calibration=0.0019*eye(3,4);
end
if  (2 == exist('Qstate.mat'))
    load Qstate.mat   % Retrieves the quat state from the last run of this script. 
else
    disp('Qstate file does not exist.  May take a while for the yaw (heading) to converge.')
    q=[1;0;0;0];    % Initial orientation quaternion.
end
%%
%   Arduino uses DTR,DTS for reset.
%
s_port=serial(['COM',num2str(port)],'BaudRate', 57600,'DataBits',8,'DataTerminalReady','on','RequestToSend','on');
if (s_port.status=='closed')
    s_port.InputBufferSize=5120;  % Make it big enough so we don't drop chars while searching for header.
    fopen(s_port);
    disp('Wait for Arduino to boot up and start transmitting.');
    %%
    %   Initialize the real time plots.
    %
    figure(1);
    subplot(3,1,1)  % Accels in top windows.

    t=[0:1/Fupdate:Twindow-1/Fupdate];  % Best guess for time.
    
    hold off
    haccelx=plot(t,0*t,'r');
    title('Real time accel');
    hold on
    haccely=plot(t,0*t,'g');
    haccelz=plot(t,0*t,'b');
    legend('x','y','z');
    axis([0,Twindow,[-2,+2]]);

    subplot(3,1,2)  % Gyros in middle windows.
    hold off
    hgyrox=plot(t,0*t,'r');
    title('gyro');
    hold on
    hgyroy=plot(t,0*t,'g');
    hgyroz=plot(t,0*t,'b');
%    legend('x','y','z');
    axis([0,Twindow,[-10,+10]]);
    
    subplot(3,1,3)  % Digital Compass in middle windows.
    hold off
    hmagnx=plot(t,0*t,'r');
    title('compass');
    hold on
    hmagny=plot(t,0*t,'g');
    hmagnz=plot(t,0*t,'b');
%    legend('x','y','z');
    axis([0,Twindow,[-1.2,+1.2]]);
    xlabel('scrolling time [sec]')
    
    accel=zeros(size(t,2),3);
    gyro =zeros(size(t,2),3);
    magn =zeros(size(t,2),3);    
    t_samples=zeros(size(t,2),1);
    YPR=zeros(size(t,2),3);  
    
    %ypr_arduino=zeros(size(t,2),3);  
    
    figure(2)
    hold off;    hrpy_x=plot(t,0*t,'r'); hold on;
    title('Orientation');
    hrpy_y=plot(t,0*t,'g');
    hrpy_z=plot(t,0*t,'b');
    xlabel('[sec]');
    ylabel('[degrees]');
    legend('Roll','Pitch','Yaw');
    axis([0,Twindow,[0,360]]);
    
    %
    %   Read the initialization strings from the Arduino.
    %
    stream_header;    
    if isempty(s_port)
        return; % Abort.
    end
    FS = stoploop({'Starting main loop.  Hit any key to exit.', ''}) ;
    
    %   Handle the packet as a header, length and binary 16 bit values.  
    %   Initially, we need to examine the data one byte at a time to find the header.
    %
    maxbytesAvailable=0;
    halfT=0.5/Fupdate;   % Initial guess at half the sample period. (Should use timestamp.)
    i=1;
    tic
    while(i<=STREAM_N)       % Check if the loop has to be stopped
        %
        %   Read and verify the header.
        %        
        header=fread(s_port,1,'uchar');
        if (header == hex2dec('FA'))        
            %
            %   Found the streaming command, check the length.
            %   There are two different streams.  1) 9DOF measurements plus timestamp 2) Optional stream which also include encoded YPR
            %   and quaternion values.
            %
            byte_count=fread(s_port,1,'uchar');
            if (20 == byte_count) || (34 == byte_count)
                %
                %   Read the payload portion of the packet.
                %
                payload=fread(s_port,(byte_count/2),'int16')';
                %
                %   These measurements correspond to the board orientation
                %   of x,y,z. (RHR, z points up).  AN3192 deals with
                %   aircraft notation where z points down.
                %
                accel(i,:)=[payload(1:3),1]*accel_calibration';      % Scale to m/s^2 with calibration values.
                gyro(i,:)= payload(4:6)/14.375*(pi/180);             % Scale to rad/sec.                
                magn(i,:)= [payload(7:9),1]*compass_calibration';    % Calibrate and normalize.
                %
                %   Update the time period with the time stamp.
                %
                halfT(i) = payload(10) / 2e6;                
                if (i>1)
                    t_samples(i)=t_samples(i-1)+payload(10)/1e6;  % Sample time in seconds with t=0 at start of collection.
                else
                    t_samples(1)=0;
                end
                if (34 == byte_count)
                    %
                    %   Sending YPR and Q calculated on Arduino.
                    %
                    q_arduino(i,:)=  payload(11:14)/32767.0;
                    ypr_arduino(i,:)=payload(15:17)/182.0;    
                    r=find(ypr_arduino<0.0);                % Find index of elements out of range.
                    ypr_arduino(r)=ypr_arduino(r)+360.0;    % Keep in range of 0 to 360 degrees.                 
                end
                %%
                %   Update AHRS algorithm.
                %
                q=AHRSUpdate(accel(i,:)',gyro(i,:)',magn(i,:)',halfT(i),q);                    
                YPR(i,:)=getYawPitchRoll(q); % Returns [Yaw, Pitch, Roll] scaled in degrees using aircraft convention.
                q_ML(i,:)=q';   % Save the results for plotting.
                %%
                %   Main window display.
                %
                switch(21)
                    case 0,     % Don't display anything. (normal mode)
                    case 1,     % Print numeric raw sensor sensor values and Display the serial backlog. For tuning Fupdate.  If PC can't keep up BytesAvailable will increase.
                        disp(sprintf('%05d Accel(%+5d,%+5d,%+5d) Gyro(%+5d,%+5d,%+5d) Mag(%+5d,%+5d,%+5d) Time %5.3f msec BytesAvailable %d\n',i,payload(1:9),payload(10)/1000,s_port.BytesAvailable));
                    case 2,     % Display the serial backlog. For tuning Fupdate.  If PC can't keep up BytesAvailable will increase.
                        disp(sprintf('%d (%d)\n',i,s_port.BytesAvailable));
                    case 10,     % Print calibrated sensor sensor values.
                        disp(sprintf('Accel(%+5.2f,%+5.2f,%+5.2f) Gyro(%+5.2f,%+5.2f,%+5.2f) Mag(%+5.2f,%+5.2f,%+5.2f) Time %5.3f msec\n',accel(i,:),gyro(i,:),magn(i,:)));
                    case 20,     %
                        if (34==byte_count)
                            %disp(sprintf('YPR(%+5.2f,%+5.2f,%+5.2f) Q(%4.3f,%4.3f,%4.3f,%4.3f) ypr(%+5.2f,%+5.2f,%+5.2f) q(%4.3f,%4.3f,%4.3f,%4.3f)',YPR(i,:),q,ypr_arduino(i,:),q_arduino(i,:)));
                            disp(sprintf('YPR(%+5.2f,%+5.2f,%+5.2f) Q(%4.3f,%4.3f,%4.3f,%4.3f) ypr(%+5.2f,%+5.2f,%+5.2f)',YPR(i,:),q,ypr_arduino(i,:)));
                        else
                            disp(sprintf('Yaw %+5.2f Pitch %+5.2f Roll %+5.2f Q(%4.3f,%4.3f,%4.3f,%4.3f)',YPR(i,:),q));
                        end
                    case 21,     %
                        if (34==byte_count)
                            %disp(sprintf('YPR(%+5.2f,%+5.2f,%+5.2f) Q(%4.3f,%4.3f,%4.3f,%4.3f) ypr(%+5.2f,%+5.2f,%+5.2f) q(%4.3f,%4.3f,%4.3f,%4.3f)',YPR(i,:),q,ypr_arduino(i,:),q_arduino(i,:)));
                            disp(sprintf('YPR(%+5.2f,%+5.2f,%+5.2f) ypr(%+5.2f,%+5.2f,%+5.2f) BytesAvailable %d',YPR(i,:),ypr_arduino(i,:)));
                        else
                            disp(sprintf('Yaw %+5.2f Pitch %+5.2f Roll %+5.2f BytesAvailable %d',YPR(i,:),s_port.BytesAvailable));
                        end
                    otherwise
                        disp('What is this?');                   
                end
                %%
                %  Update the graph display.
                %
                if (0==mod(i,100))
                    %%
                    %   We don't have enough CPU to update the plots every sample update.
                    %   Tune this update for your computer.
                    %                
                    if (i <= (Fupdate*Twindow))
                        range=[1:floor(Fupdate*Twindow)];    % Plot the first Twindow seconds of data. (zero filled)
                    else
                        range=[i-floor(Fupdate*Twindow)+1:i];  % Plot the most recent Twindow seconds of data.
                    end
                    %
                    %   Update the data directly.
                    %                    
                    set(haccelx,'YData',accel(range,1));
                    set(haccely,'YData',accel(range,2));
                    set(haccelz,'YData',accel(range,3));

                    set(hgyrox, 'YData',gyro(range,1));
                    set(hgyroy, 'YData',gyro(range,2));
                    set(hgyroz, 'YData',gyro(range,3));

                    set(hmagnx, 'YData',magn(range,1));
                    set(hmagny, 'YData',magn(range,2));
                    set(hmagnz, 'YData',magn(range,3));
                    if (34==byte_count)
                        %
                        %   Update the Roll, Pitch, Yaw plot.
                        %                    
                        set(hrpy_x, 'YData',ypr_arduino(range,3));
                        set(hrpy_y, 'YData',ypr_arduino(range,2));
                        set(hrpy_z, 'YData',ypr_arduino(range,1));
                    end
                    drawnow
                    %%
                    %   Checking for keypresses takes time so only check when updating the display.
                    %
                    if (FS.Stop())
                        i=STREAM_N;
                    end
                    %
                    %   Update the model.
                    %
                    %moveModel_switch=4;
                    %MoveModel      % Call script to animate the model.              
                end
                i=i+1;
            else
                disp(['Wrong byte count ',num2str(byte_count)]);
                i=STREAM_N+1;   % Exit the loop.
            end
        else    % Wrong header character.
            if (i>1)
                accel(i,:)=zeros(1,3);
                gyro(i,:)=zeros(1,3);
                magn(i,:)=zeros(1,3);                
                t_samples(i)=t_samples(i-1); % Sample time in seconds with t=0 at start of collection.
            else
                t_samples(i)=0;
            end
            i=i+1;
            disp(sprintf('OOS %d value %s %c BAvailable %d.\n',i,dec2hex(header),header,s_port.BytesAvailable));
   %         i=STREAM_N+1;   % Exit the loop.
            %%
            %   Checking for keypresses takes time so only check when updating the display.
            %
            if (FS.Stop())
                i=STREAM_N;
            end
        end
    end
    %%
    %   Save the data from this run.
    %
    filename=['IMU_data_',datestr(now),'.mat'];
    filename(strfind(filename,':'))='_';    % Replace : with _.
    filename(strfind(filename,' '))='_';    % Replace space with _.
    filename=[filename(1:size(filename,2)-7),'.mat'];   % Drop seconds and restore .mat.
    save(filename,'t_samples','accel','gyro','magn','q_ML','YPR');
    if (1==exist('ypr_arduino','var'))
        save(filename,'ypr_arduino','-append');
    end
    %
    %   Save the orientation so we have a starting point.  
    %   However, we are not currently saving exInt, eyInt, ezInt AHRS internal state variables.
    %
    save('Qstate.mat','q'); % Orientation state of the current position for the next run.
    %%
    %   Clean up the loop exit.
    %
    FS.Clear() ;  % Clear up the box
    clear FS ;    % This structure has no use anymore.    
    %%
    %   Close down the serial port.
    %
    ttt=toc;
    
    fclose(s_port)
    disp(sprintf('%d packets length %d took %4.2f seconds, update %3.1f hz.',size(t_samples,1),byte_count+2,ttt,size(t_samples,1)/ttt));
else
    disp('serial port not closed.')
end
