%
%   Simple plot of several variables over the whole time span.
%   t_samples Time in msec the samples were taken.
%   accel - acceleration in G.
%   gyro - angular velocity in rad/sec.
%   magn - normalized magnetic vector.  Use the horizontal plane components of these values.
%   YPR - Yaw, Pitch, Roll values calculated from the acceleration and magnetic sensors. (valid for static and slow moving.
%
figure(1)
clf
if (~isempty(gyro))
    subplot(3,1,1)
    plot(t_samples,accel(:,1),'r')
    title('Raw accel');
    ylabel('G');
    hold on
    plot(t_samples,accel(:,2),'g')
    plot(t_samples,accel(:,3),'b')
    legend('x','y','z');
    hold off

    subplot(3,1,2)
    plot(t_samples,gyro(:,1),'r')
    title('Raw gyro');
    ylabel('rad/sec');
    hold on
    plot(t_samples,gyro(:,2),'g')
    plot(t_samples,gyro(:,3),'b')
    %legend('x','y','z');
    hold off

    subplot(3,1,3)
    plot(t_samples,magn(:,1),'r')
    title('Raw compass');
    xlabel('sec');
    ylabel('normalized');
    hold on
    plot(t_samples,magn(:,2),'g')
    plot(t_samples,magn(:,3),'b')
    %legend('x','y','z');
    hold off
else
    plot(t_samples,accel(:,1),'r')
    title('Raw accel');
    ylabel('G');
    hold on
    plot(t_samples,accel(:,2),'g')
    plot(t_samples,accel(:,3),'b')
    legend('x','y','z');
    hold off
end
if (0)
    %%
    %   Plot the digital compass readings.
    %   Should look like points on surface of a sphere if the sensor is rotated.
    figure(99)
    clf
    plot3(magn(:,1),magn(:,2),magn(:,3))
    title('Compass');   xlabel('X');ylabel('Y');zlabel('Z');    grid on;    axis equal;

    if (0)
        %
        %  Find average size of the sphere and display it (inside all the points)
        %
        r=sum(sqrt(sum(magn.*magn,2)))/size(magn,1)* 0.90;    % Scale a little bit smaller so sphere doesn't hide points.
        [x,y,z]=sphere(360);   % Set of points magnetic sphere.
        x=r*x; y=r*y; z=r*z;    % Scale the sphere.

        surf(x,y,z,'EdgeColor','none');
    end
    if (0)
        %%
        %   Plot 2D versions. Useful during calibration.
        %
        figure(77)
        plot(magn(:,1),magn(:,2))
        title('Compass XY');    xlabel('X');    ylabel('Y');    grid on;   axis equal

        figure(78)
        plot(magn(:,2),magn(:,3))
        title('Compass YZ');    xlabel('Y');    ylabel('Z');    grid on;   axis equal

        figure(79)
        plot(magn(:,1),magn(:,3))
        title('Compass XZ');    xlabel('X');    ylabel('Z');    grid on    axis equal
    end
end

if (1==exist('YPR','var'))
    %%
    %   Plot Yaw,Pitch,Roll over entire buffer.
    %
    figure(2)
    clf
    if 0 && (1==exist('ypr_arduino','var'))
        %
        %   If we have values calculated with the arduino microcontroller, plot them as well.
        %
        subplot(2,1,2)
        plot(t_samples,ypr_arduino(:,1),'r')
        title('Arduino');
        xlabel('sec');
        ylabel('degrees');
        hold on
        plot(t_samples,ypr_arduino(:,2),'g')
        plot(t_samples,ypr_arduino(:,3),'b')
        legend('yaw','pitch','roll');
        hold off
        grid on

        subplot(2,1,1)
    end
    plot(t_samples,YPR(:,1),'r')
    title('Matlab AHRS');
    xlabel('sec');
    ylabel('degrees');
    hold on
    plot(t_samples,YPR(:,2),'g')
    plot(t_samples,YPR(:,3),'b')
    legend('yaw','pitch','roll');
    hold off
    grid on
end
