%%
%   t_samples contains the timestamp in seconds.
%   accel contains the 3D acclerations in Gs.
%   YPR contains the Yaw, Pitch, Roll in degrees.
%   known_velocity is an optional array of the known_velocity for initialization and plotting.
%   known_position is an optional array of the known_position for initialization and plotting.
%
function [position]=test_position(t_samples,accel,YPR,known_velocity,known_position)
%%
%   Integrates acceleration and orientation into position.
%
clear Calculate_Position    % Clear persistent variables inside the function.

%%
%   Perform double integration over the movement time period.
%
if (0)
    %%
    %   Simpson's Rule Integration (3-point Newton-Cotes quadrature rule.)
    %   (b-a)/6*(f(a) + 4*f((a+b)/2) + f(b))
    %
    N=size(accel,1);
    velocity=zeros(N/2,3);    % Variable containing the 3D velocity.
    position=zeros(N/4,3);    % Return variable containing the 3D position relative to the initial position.
        
    for (s=2:2:size(accel,1))
        %
        %   Acceleration rotations goes here.
        %
        %   Use orientation to rotate acceleration back to stationary frame.
        %   ypr contains Yaw, Pitch, Roll.  Apply in order of roll, pitch, roll to get to fixed frame coordinates.
        %
        if (0)
            rotation_x=t1r1( ypr(3,1),0,0);     % Roll Rotation about x. 
            rotation_y=t1r1(-ypr(2,1),0,1);     % Pitch rotation about y. Notice sign reversal to get relative to IMU rather than aircraft convention.
            rotation_z=t1r1(-ypr(1,1),0,2);     % Yaw rotation about z.  Notice sign reversal to get relative to IMU rather than aircraft convention.
            %
            %   This matrix transforms the IMU coordinates to stationary XYZ frame. Fixed XYZ frame.
            %
            R=rotation_z(:,1:3)*rotation_y(:,1:3)*rotation_x(:,1:3);    % Only need the rotation portion of the translation matrix.
        end
        
        if (0==mod(s,2))   % Only update velocity every other sample.
            if (2==s)
                delta_t=t_samples(2);   
                fa=0;   % Initial acceleration condition at t=0.
                fab=9.81*accel(s-1,:)-[0,0,9.81];    
                fb =9.81*accel(s  ,:)-[0,0,9.81]; 
                
                velocity(s/2,:)=known_velocity(1,:) + (fa+4*fab+fb)/6*delta_t;  % Use initial value.
            else
                delta_t=t_samples(s)-t_samples(s-2);
                fa= 9.81*accel(s-2,:)-[0,0,9.81];    
                fab=9.81*accel(s-1,:)-[0,0,9.81];    
                fb =9.81*accel(s  ,:)-[0,0,9.81]; 
    
                velocity(s/2,:)=velocity(s/2-1,:) + (fa+4*fab+fb)/6*delta_t;
            end            
            %
            %   Only half as many velocity values as acceleration.
            %   Half again the number of position points.
            %
            if (0 == mod(s,4)) % Only calculate position every fourth sample.
                if (4==s)
                    delta_t=t_samples(s);
                    fa =known_velocity(1,:); % Initial velocity condition.
                    fab=velocity(s/2-1,:); % Previous velocity value.
                    fb =velocity(s/2,:);   % Newest value.
                    position(s/4,:)=known_position(1,:) + delta_t/6*(fa+4*fab+fb);  % Use initial value.
                else
                    delta_t=t_samples(s)-t_samples(s-4);
                    fa =velocity(s/2-2,:); % Two velocity values ago.
                    fab=velocity(s/2-1,:); % Previous velocity value.
                    fb =velocity(s/2,:);   % Newest value.
                    position(s/4,:)=position(s/4-1,:) + delta_t/6*(fa+4*fab+fb);
                end                
            else
                %   Position doesn't update this sample.
            end
        else
            % Velocity doesn't update this sample.
        end
    end
else
    %
    %   One sample at a time.
    %
    t_average=(t_samples(size(accel,1))-t_samples(1))/(size(accel,1)-1);    % Average sample time.
    for (i=1:size(accel,1))
        if (1 == i)
            v_0=known_velocity(1,:)';
            p_0=known_position(1,:)';
            %
            %   First calculation passes the initial conditions.
            %
            [p,v]=Calculate_Position(9.81*accel(i,:)',YPR(i,:)',t_average,v_0,p_0);   % Assumes the samples are evenly (t_average) spaced in time. 
        else
            [p,v]=Calculate_Position(9.81*accel(i,:)',YPR(i,:)',(t_samples(i) - t_samples(i-1)));  % After i=1, we know the delta time.
        end
        position(i,:)=p';   % Keep the calculated position.            
        velocity(i,:)=v';
        
%        disp(sprintf('Yaw %6.2f Pitch %6.2f Roll %6.2f',YPR(i,:)));
%        disp(sprintf('P=(%6.4f %6.4f %6.4f)',p));
    %    pause(0.01);        
    end
end

if (0)
    %%
    %   Integrate reverse order which should return to original starting point.
    %
    for (i=size(accel,1)-1:-1:1)
        p=Calculate_Position(9.81*accel(i,:)',YPR(i,:)',(t_samples(i+1) - t_samples(i)))';
        %
        %   Use orientation to rotate acceleration back to stationary frame.
        %   ypr contains Yaw, Pitch, Roll.
        %
        rotation_x=t1r1( YPR(i,3),0,0);     % Roll Rotation about x. 
        rotation_y=t1r1(-YPR(i,2),0,1);     % Pitch rotation about y. Notice sign reversal to get relative to IMU rather than aircraft convention.
        rotation_z=t1r1(-YPR(i,3),0,2);     % Yaw rotation about z.  Notice sign reversal to get relative to IMU rather than aircraft convention.
        %
        %   This matrix transforms the IMU coordinates to orientation vector.
        %
        orientation(i,:)=rotation_z(:,1:3)*rotation_y(:,1:3)*rotation_x(:,1:3)*[1;0;0];    % Only need the rotation portion of the translation matrix.
        position(i,:)=p;   % Keep the calculated position.
    end
end

if (1)
    %%
    %   Plot acceleration, velocity, and position.
    %
    figure(64)
    clf
    if (size(accel,1)==size(position,1))
        t_samples_2=t_samples;
        t_samples_4=t_samples;
    else
        t_samples_2=t_samples(1:2:size(t_samples,2));
        t_samples_4=t_samples(1:4:size(t_samples,2));
    end
        
    subplot(3,1,1)
    plot(t_samples,accel(:,1),'r--')
    hold on
    title('Acceleration Input');
    ylabel('[m/sec^2]');
    grid on;
    plot(t_samples,accel(:,2),'g')
    plot(t_samples,accel(:,3),'b')

    subplot(3,1,2)
    plot(t_samples_2,velocity(:,1),'r')
    hold on
    title('Velocity estimate');
    ylabel('[m/sec]');
    grid on;
    plot(t_samples_2,velocity(:,2),'g')
    plot(t_samples_2,velocity(:,3),'b')

    subplot(3,1,3)
    plot(t_samples_4,position(:,1),'r')
    hold on
    title('Position estimate');
    xlabel('[sec]');
    ylabel('[m]');
    grid on;
    plot(t_samples_4,position(:,2),'g')
    plot(t_samples_4,position(:,3),'b')

    figure(65)
    clf
    plot3(position(:,1),position(:,2),position(:,3),'k')
    hold on
    title('Position Estimate in 3D Space')
    xlabel('x[m]');
    ylabel('y[m]');
    zlabel('z[m]');
    axis square
    grid on
end

end     % test_position().

    