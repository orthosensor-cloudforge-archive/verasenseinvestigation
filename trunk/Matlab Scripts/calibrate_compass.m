%
%   This script implements the digital compass calibration method detailed in AN3192
%   http://www.st.com/stonline/products/literature/an/17353.pdf
%
%%
%   Reads serial stream from Aruduino 9DOF board during 2D rotations and LS calculates the calibration
%   values.
%   The data from the 2D rotations is used to determine:
%   M_m -  3x3 misalignment matrix.
%   M_SC - 3x3 diagonal scale matrix.
%   M_si - 3x3 Soft iron distortion matrix.
%   M_OS - 3x1 Hard iron offset vector.
%
%   M - 3x1 vector of raw magnetic values can be converted to normalized
%   values using:
%   M_calibrated=M_m*M_SC*M_si[M-M_OS]
%  
%   or
%   compass_calibration=[M_m*M_SC*M_si, M_m*M_SC*M_si*-M_OS]
%   M_calibrated=compass_calibration * [M;1]
    
disp(['Orthosense Digital Compass calibration.']);
clear YPR;
t_samples=[];
accel=[];
gyro=[]; 
magn=[];
%
%   Position indicates which axis is pointed up.  The IMU is rotated in
%   horiz ontal plane perpendicular to this axis.
%
positions=[
    [+1,0,0];
    [ 0,+1,0];
    [ 0,0,+1]];
H=[];
w=[];

N=1000;   % Number of measurements for each 2D rotation.
%
%   Collect measurements at each position.
%   This formulation is valid for neglible soft iron distortion uses
%   M_si=I.
%
for (p=1:size(positions,1))
    disp(['Rotate IMU about this axis [',num2str(positions(p,:)),'].'])
    disp('Type any key to continue');
    pause
    if (1)
        %
        %   Measurements. (accel, gyro, magn, time).
        %
        [a,g,m,t]=collect_9DOM(port,N);
    else
        %
        %   Simulated values.
        %
        phase=[2*pi/N:2*pi/N:2*pi]';
        m(:,1)=cos(phase);
        m(:,2)=sin(phase);
        m(:,3)=-3*ones(N,1)+0.1*rand(N,1);
    end
    %
    %   Fill w and H with values measured during the rotation.
    %
    w=[w;m(:,1).^2]
    H=[H;[m, -m(:,2).^2, -m(:,3).^2,ones(N,1)]]
    
    %
    %   Collect all the measurements (for plotting).
    %
    accel=[accel;a];
    gyro=[gyro;a];
    magn=[magn;m];
    t_samples=[t_samples;t];
    %
    %   These are intermediate uncalibrated results.
    %
    stream_plot;
end

%%
%  LS solution for compass offset calibration parameters.
%
X=lscov(H,w)

%%
%
%
M_si=eye(3);    % Neglect soft iron distortion. (Will show up as tilted ellipse if present.)
%%
%   Extract the hard iron offset calibration from the LS solution.
%
M_OS(1,1)=X(1)/2;
M_OS(2,1)=X(2)/(2*X(4));
M_OS(3,1)=X(3)/(2*X(5));
%   
%   Extract the scale factors from the LS solution.
%
A=X(6) + M_OS(1)^2 + X(4)*M_OS(2)^2 + X(5)*M_OS(3)^2;
B=A/X(4);
C=A/X(5);
M_SC=inv(diag([sqrt(A),sqrt(B),sqrt(C)]))   % Notice M_SC is saved as inverse.
%%
%   Determine the misalignment between the sensor axis and the body axis.
%   Use data from each 2D rotation.
%
A=H(:,1:3)-ones(size(positions,1)*N,1)*M_OS';  % Remove offset.
A(:,1)=A(:,1)*M_SC(1,1);    % Scale.
A(:,2)=A(:,2)*M_SC(2,2); 
A(:,3)=A(:,3)*M_SC(3,3);
%
%   A now contains [xxx,yyy,zzz] for all the 2D rotations.
%   Use first N measurements to find Rx. (normalized misalignment)
%
rows=1:N;
w=sqrt(sum(A(rows,:).*A(rows,:),2));  % sqrt(xxx^2+yyy^2+zzz^2)
X=lscov(A(rows,:),w)
Rx=X/norm(X);

rows=N+1:2*N;
w=sqrt(sum(A(rows,:).*A(rows,:),2));  % sqrt(xxx^2+yyy^2+zzz^2)
X=lscov(A(rows,:),w)
Ry=X/norm(X);

rows=2*N+1:3*N;
w=sqrt(sum(A(rows,:).*A(rows,:),2));  % sqrt(xxx^2+yyy^2+zzz^2)
X=lscov(A(rows,:),w)
Rz=X/norm(X);

M_m=[Rx,Ry,Rz]

%
%   Rearrange terms into a single 3x4 matrix of calibration values.
%
compass_calibration=[M_m*M_SC*M_si, M_m*M_SC*M_si*-M_OS]

%
%   Save calibration values in the file.
%
compass_calibration=-compass_calibration;   % I think this is needed because App note derives for aerospace convention.
save('compass_calibration.mat','M_SC','M_OS','M_m','compass_calibration');


%%
%   These are calibrated results.
%
accel_uncalibrated=accel;
magn_uncalibrated=magn;
stream_plot;

accel=[accel,ones(size(positions,1)*N,1)]*accel_calibration';
magn= [magn ,ones(size(positions,1)*N,1)]*compass_calibration';
stream_plot;