%%
%   Renders a circular xy plane transformed by T. Similar to RenderArc.
%
function [ output_args ] = RenderCircularPlane(T,Radius,zShift)
        rad=[-180:20:+180]'*(pi/180); % Angle in radians.
        vertices = [[0,0,0];cos(rad), sin(rad), zeros(length(rad),1)] * Radius;        
        vertices = tmultp(T,tmultp(t1r1(0,zShift,2),vertices'))';  % Shift out the z axis and transform with rotations.        
        faces = [ ones(length(vertices)-2,1), [2:length(vertices)-1]', [3:length(vertices)]'];

        handle = patch('Faces',faces,'Vertices',vertices);
        set(handle,'FaceColor','b','EdgeColor','b')
        set(handle,'FaceAlpha',0.3,'EdgeAlpha',.1);

    switch(nargin)
        case 0,
        case 1
        case 2,
        case 3,
        otherwise
            assert(0,'Wrong number of arguments.')
    end
   
    
if (0)    
    
    if isempty(arcQuiverHandle) || isempty(arcPieVertices)
        %
        %   First time called, create the geometry and plot handles.
        %
        movementRadius=sin((tibiaElevationDegrees-hipElevationDegrees)*pi/180)*tibia_length;  % Radius of the surgical movement about the axis from ankle to hip joint.
        movementProjection=cos((tibiaElevationDegrees-hipElevationDegrees)*pi/180)*tibia_length;  % Projection of the surgical movement about the axis from ankle to hip joint to body yz plane.

        %%
        %   Indicate the ankle to hip joint axis. Only plot to the center of the movement arc.
        %   Notice scale=0 to avoid autoscaling.
        arcQuiverVector=[0;0;movementProjection];
        arcQuiverHandle=quiver3(0,0,0, arcQuiverVector(1),arcQuiverVector(2),arcQuiverVector(3),0,'k','LineWidth',1);

        %%
        %   Indicate the plane the circular arc is constrained to with a piece of plane(pie) covering the movement arc.
        %
        rad=[-rangeDegrees:1:+rangeDegrees]'*(pi/180);
        vertices = [[0,0,0];cos(rad), sin(rad), zeros(length(rad),1)] * movementRadius;        
        arcPieVertices = tmultp(T,tmultp(t1r1(0,movementProjection,2),vertices'))';  % Shift along the z axis.
        
        faces = [ ones(length(arcPieVertices)-2,1), [2:length(arcPieVertices)-1]', [3:length(arcPieVertices)]'];
        arcPieHandle = patch('Faces',faces,'Vertices',arcPieVertices,'FaceColor','r','FaceAlpha',0.3,'EdgeColor',[.1,0,0],'EdgeAlpha',.1);
    else
        %
        %   Subsequent calls update the plots using persistent data.
        %
        get(arcQuiverHandle,'WData');
        uvw=tmultp(T,arcQuiverVector);
        set(arcQuiverHandle,'UData',uvw(1),'VData',uvw(2),'WData',uvw(3));

        xyz=tmultp(T,arcPieVertices');
        set(arcPieHandle,'XData',xyz(1,:)','YData',xyz(2,:)','ZData',xyz(3,:)');
    end
end
    output_args=[];
end

