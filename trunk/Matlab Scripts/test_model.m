%%
%   Initialize the 'robotic' model for hip.
%
hipjointmodel

%%
%
%
if (0==mod(i,15))
        %%
        %   Robotic model update.
        %   Find transformation for these joint variables.
        %        
        JTable(1,4)=YPR(i,1);
        JTable(2,4)=-YPR(i,2);
        %%
        %   Get the Transforms ready for the joint variables.
        %   These transform points from the ith frame to the zeroth.
        %
        T01=tofi(JTable(1,1),JTable(1,2),JTable(1,3),JTable(1,4));
        T02=tmultt(T01,tofi(JTable(2,:)));
        T03=tmultt(T02,tofi(JTable(3,:)));
        T04=tmultt(T03,tofi(JTable(4,:)));
        T05=tmultt(T04,tofi(JTable(5,:)));
        %%
        %   Transform all the model points.
        %   Update the plot information.
        %
        [x,y,z]=tmultp3D(T02,hip_ball_jointX_0,hip_ball_jointY_0,hip_ball_jointZ_0);    % Transform from ith to zero frame coordinates.
        set(hip_ball_handle,    'XData',x,'YData',y,'ZData',z);

        [x,y,z]=tmultp3D(T02,f1_linkX_0,f1_linkY_0,f1_linkZ_0);
        set(f1_link_handle,     'XData',x,'YData',y,'ZData',z);

        [x,y,z]=tmultp3D(T03,f2_linkX_0,f2_linkY_0,f2_linkZ_0);
        set(f2_link_handle,     'XData',x,'YData',y,'ZData',z);

        [x,y,z]=tmultp3D(T03,j_x_0,j_y_0,j_z_0);
        set(j_radius_handle,    'XData',x,'YData',y,'ZData',z);

        [x,y,z]=tmultp3D(T04,knee_jointX_0,knee_jointY_0,knee_jointZ_0);
        set(knee_joint_handle,  'XData',x,'YData',y,'ZData',z);

        [x,y,z]=tmultp3D(T05,razor_x_0,razor_y_0,razor_z_0);
        set(razor_handle,       'XData',x,'YData',y,'ZData',z);
end
