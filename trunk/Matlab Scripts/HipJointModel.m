%%
%   Matlab script to display a hip joint model using robotics conventions.
%   
%   This script is develops the forward or configuration kinematic equations. (determine position and orientation of knee given
%   the joint variables.)
%   This is useful for testing ideas on how to solve the inverse kinematics problem of solving for joint variables given
%   observations of the knee position and orientation.
%

%%
%   Describe the model geometry.
%
%   2 joints describe the ball and socket hip joint. (zero length link between)
%   1 link at the ball and socket is zero length to allow the 2DOF joint to be modeled as 2 1DOF joints.
%   1 link models the skeleton link between the hip joints and the IMU.
%   1 joint models the attachment of the IMU to the femur.

%
%   Dimensions of the model. [meters]
%
hip_joint_radius=0.03;
femur_link_radius=0.02;
f1_link_length=0.10;
f1_link_radius=0.01;
f2_link_radius=0.01;
f2_link_length=0.45;
knee_joint_radius=0.03;

femur_length=sqrt(f1_link_length^2+f2_link_length^2);

%%
%   Joint parameters for the hip joint model.
%   aim1 alpha_i dzi theta_i
%   
JTable=[
    0                0      0               0;   % Frame 1 - R 1D Rotation of ball and socket.
    0              -90      0               0;   % Frame 2 - R 1D Rotation of ball and socket.
    0                0      f1_link_length 90;   % Frame 3 - P Prismatic joint.
    0              +90      f2_link_length  0;   % Frame 4 - P Prismatic joint.
-knee_joint_radius  0      0               0;   % Frame 5 - R Fixed IMU mount on femur.
];
T01=tofi(JTable(1,1),JTable(1,2),JTable(1,3),JTable(1,4));
T02=tmultt(T01,tofi(JTable(2,:)));
T03=tmultt(T02,tofi(JTable(3,:)));
T04=tmultt(T03,tofi(JTable(4,:)));
T05=tmultt(T04,tofi(JTable(5,:)));

%   Describe points (origin and 3 unit axis) to illustrate the coordinate axis. 
%   Then transform a set of unit axis for each frame.
%
units0=0.15*[0 0 0; 1 0 0; 0 1 0;  0 0 1]';

%%
%   Describe Geometry of each Link relative to its own frame.
%
%%
%   The hip ball and socket joint for this diagram is treated as the reference point. (0,0,0)
%   In practice, this is the location we will solve for.
%   The ball and socket two degree of freedom joint treated as two single DOF joints.
%
[x,y,z]=sphere(15);   % Set of points for 1cm sphere representing the hip joint.
hip_ball_jointX_0=hip_joint_radius*x;
hip_ball_jointY_0=hip_joint_radius*y;
hip_ball_jointZ_0=hip_joint_radius*z;
%
%   Determine color of the sphere by quadrant.
%
for (i=1:3)
    color(:,:,i)=((sign((x+.001).*(y+.001).*(z+.001)))+1)/2;   % Defines color as a triple.    
end
color(color~=0)=.5; % Add some color to the white faces.

%%
%   The femur is modeled as two rigid links at 90 degrees.
%   Each link is represented by a cylinder with a specified radius and length.
%
[f1_linkX_0,f1_linkY_0,z]=cylinder(f1_link_radius,15);  % Set of points for hip bone1.
f1_linkZ_0=f1_link_length*z;                            % Scale the length.

[f2_linkX_0,f2_linkY_0,f2_linkZ_0]=cylinder(f2_link_radius,15); % Set of points for hip bone2.
f2_linkZ_0=f2_link_length*f2_linkZ_0;                           % Scale length.
[f2_linkX_0,f2_linkY_0,f2_linkZ_0]=tmultp3D(t1r1(90,0,0),f2_linkX_0,f2_linkY_0,f2_linkZ_0);
%%
%   Draw another sphere to smooth connection of the two link cylinders.
%
j_radius=max(f1_link_radius,f2_link_radius);
[x,y,z]=sphere(15);   % Set of points for sphere representing the joint.
j_x_0=j_radius*x;
j_y_0=j_radius*y;
j_z_0=j_radius*z;
%%
%   Knee Joint (could use two spheres close together).
%
[x,y,z]=sphere(15);          % Set of points for sphere representing the knee joint.
knee_jointX_0=knee_joint_radius*x;
knee_jointY_0=knee_joint_radius*y;
knee_jointZ_0=knee_joint_radius*z;
%%
%   Razor center uses bottom of board half the width and length.
%   28 x 41 x 1.5875 mm
%
rw=0.028;     % Razor length/2 in meters.
rl=0.041;     % Razor width/2 in meters.
rh=0.0015875;   % Razor height in meters.

[x,y,z]=cylinder(sqrt(2),4);  % Set of points for razor board.  Note 4 sided cylinder returns rectangle at 45 degrees.
[x,y,z]=tmultp3D(t1r1(45,-0.5,2),x,y,z);    % Rotate 45 degrees in native coordinates to correct for cylinder 45 degrees and offset half the height to center.

razor_x_0=x*rh/2;
razor_y_0=z*rl;     % Rectangle is half the other dimensions.
razor_z_0=y*rw/2;

%%
%   Set up the 3D Plot view.  
%
axis equal
r=sqrt(f1_link_length^2 + f2_link_length^2)+knee_joint_radius;
axis([-r,+r,-r,+r,-r,+r]);

%% 
%   Standard view.
%
figure(100)
hold off
%%
%   Transform points to zero coordinatea and plot.
%
[x,y,z]=tmultp3D(T02,hip_ball_jointX_0,hip_ball_jointY_0,hip_ball_jointZ_0);    % Transform from ith to zero frame coordinates.
hip_ball_handle=surf(x,y,z,color,'EdgeColor','none');
hold on
title('Hip Location and Orientations.');
xlabel('X_0')
ylabel('Y_0')
zlabel('Z_0')
view(80,10)
axis equal
axis([-r,+r,-r,+r,-r,+r]);

%
%   Display Coordinate axis for each frame.
%
if (1)
    for (j=1:size(JTable,1))

        units=tmultp(t0i(j,JTable),units0);

        quiver_handle_x(j)=quiver3(units(1,1),units(2,1),units(3,1), (units(1,2)-units(1,1)), (units(2,2)-units(2,1)), (units(3,2)-units(3,1)),'r','LineWidth',1);
        quiver_handle_y(j)=quiver3(units(1,1),units(2,1),units(3,1), (units(1,3)-units(1,1)), (units(2,3)-units(2,1)), (units(3,3)-units(3,1)),'g','LineWidth',1);
        quiver_handle_z(j)=quiver3(units(1,1),units(2,1),units(3,1), (units(1,4)-units(1,1)), (units(2,4)-units(2,1)), (units(3,4)-units(3,1)),'b','LineWidth',1);
x=get(quiver_handle_x(j),'XData')
y=get(quiver_handle_x(j),'YData')
z=get(quiver_handle_x(j),'ZData')
    end
end

[x,y,z]=tmultp3D(T02,f1_linkX_0,f1_linkY_0,f1_linkZ_0);
f1_link_handle=mesh(x,y,z);

[x,y,z]=tmultp3D(T03,f2_linkX_0,f2_linkY_0,f2_linkZ_0);
f2_link_handle=mesh(x,y,z);

[x,y,z]=tmultp3D(T03,j_x_0,j_y_0,j_z_0);
j_radius_handle=mesh(x,y,z);

[x,y,z]=tmultp3D(T04,knee_jointX_0,knee_jointY_0,knee_jointZ_0);
knee_joint_handle=mesh(x,y,z);

[x,y,z]=tmultp3D(T05,razor_x_0,razor_y_0,razor_z_0);
razor_handle=mesh(x,y,z);

if (0)
    figure(102)
    clf
    title('Joint Variables');
    legend(['theta_1','theta_2']')
    hold on
end
