
%Matlab normally uses only a single core on a single CPU, except when using the Parallel Computing Toolbox or when doing some implicit parallelization of vectorized code. Therefore, on a quad-core dual-CPU machine, we would normally see Matlab�s CPU usage at only 1/(2*4)=12%. The simplest way to utilize the unused CPU cores without PCT is to spawn additional Matlab processes. This can be done using the system function, as above. The spawned Matlab sessions can be made to run specific commands or functions. For example:
%
system('matlab �r "for idx=1:1000, x=rand(1000,1000), y=fft(x); end" &');
%system('matlab �r "for idx=1:1000, x=rand(1000,1000), y=fft(x); end" &');
%system('matlab �r "for idx=1:1000, x=rand(1000,1000), y=fft(x); end" &');
%system('matlab �r "for idx=1:1000, x=rand(1000,1000), y=fft(x); end" &');
% At this point, we may possibly wish to use processor affinity to ensure that each process runs on a separate CPU. 
% Different OSes have different ways of doing this. For example, on Windows it can easily be done using Process Explorer�s context menu.

% When Matlab spawns an external process, it passes to it the set of environment variables used in Matlab. This may be different than the set that is normally used when running the same process from the OS�s command prompt. 
% This could lead to unexpected results, so care should be taken to update such environment variables in Matlab before spawning the process, if they could affect its outcome.
%
% Once an asynchronous (non-blocking) process is started, Matlab does not provide a way to synchronize with it. 
% We could of course employ external signals or the state or contents of some disk file, to let the Matlab process know that one or more of the spawned processes has ended. When multiple processes are spawned, we might wish to employ some sort of load balancing for optimal throughput.
%
% We can use OS commands to check if a spawned processId is still running. 
% This ID is not provided by system so we need to determine it right after spawning the process. 
% On Unix systems (Linux and Mac), both of these can be done using a system call to the OS�s ps command; on Windows we can use the tasklist or wmic commands.
%
% An alternative is to use Java�s built-in process synchronization mechanism, which enables more control over a spawned process. The idea is to spawn an external asynchronous process via Java, continue the Matlab processing, and later (if and when needed) wait for the external process to complete:

