%%
%   Simulate surgical movement.
%
%   Surgical movement is driven by accel measurements. (Accel frame.)
%   Accel always measures relative to gravity, but anatomy pivots on axis from angle to hip.
%   Use "elevation" to refer angles relative to gravity. (accel values)
%
ModelGeometry;  % Initialize geometry and figure 1.

%%
%   Plot body with hip elevation.
%
global tibiaElevationDegrees hipElevationDegrees
global tibia_length femur_length tibiaGeometry

VeraSenseDegrees=[0.0, 0.0, 0.0];  % Rotation of the VeraSense unit relative to the tibia.

tibiaElevationDegrees=45;                       % Angle of tibia relative to plane normal to gravity.
hipElevationDegrees=+0.0;                       % Angle of elevation of hip relative to ankle.
rangeDegrees= 2.29;                                % Surgical movement will go from -+rangeDegrees. (As indicated by accel.)

if (1)
    %
    %   RotZ(RotY(RotX)))
    %
    VT=tmultt(t1r1(-VeraSenseDegrees(3),0,2),tmultt(t1r1(-VeraSenseDegrees(2),0,1),t1r1(-VeraSenseDegrees(1),0,0)));  % VeraSense misadjustment.
else
    %
    %   Gordon uses different order. (Needs to be translated???)
    %
    disp('Trying different rotation order to match Gordon.');
    VT=tmultt(+t1r1(VeraSenseDegreecs(3),0,2), -t1r1(VeraSenseDegrees(2),0,1));
end
VT=tmultt(t1r1(0,tibia_length,2),VT);   % Slide to knee.

campos([2,6,-5]);
if (0)
    %%
    %   Measured by VeraSense unit.
    %
    g=[1;0;0] % Gravity vector in ankle frame. 
    tmultv(tmultt([VT(:,1:3),[0;0;0]],t1r1(-tibiaElevationDegrees,0,1)), g)  % Measurement with tibia in position.
    tmultv(tmultv( VT                ,t1r1(-tibiaElevationDegrees,0,1)), g)  % Measurement with tibia in position.
    tilt=0;
    tmultv(tmultt([VT(:,1:3),[0;0;0]],tmultt(t1r1(-tibiaElevationDegrees,0,1),t1r1(-tilt,0,2))), g)  % Measurement with tibia in position.
    tmultv(tmultv( VT                ,tmultt(t1r1(-tibiaElevationDegrees,0,1),t1r1(-tilt,0,2))), g)  % Measurement with tibia in position.
    break
end
if (0)
    %
    %   Flip back and forth from Ankle to Accel frame views. (different by hipElevationDegrees rotation.)
    %
    femur_angle=-(180- (90-(tibiaElevationDegrees-hipElevationDegrees)) - acos(tibia_length*sin((tibiaElevationDegrees-hipElevationDegrees)*pi/180)/femur_length)*180/pi);
    title('Geometery in Ankle frame.');
    Tbody=t1r1(tibiaElevationDegrees-hipElevationDegrees,0,1);
    RenderTibia(tibia_geometry,Tbody);
    RenderVeraSense(tmultt(Tbody,VT));    
    %
    %   Rotate the femur down, then shift to body frame, and apply body T.
    %
    T=tmultt(Tbody,tmultt(t1r1(0,tibia_length,2),t1r1(femur_angle,0,1)));
    RenderFemur(T);
    RenderArc(eye(3,4),rangeDegrees);
    pause(1);

    title('Geometery in Accel frame.');
    Tbody=t1r1(tibiaElevationDegrees,0,1);
    RenderTibia(Tbody);
    RenderVeraSense(tmultt(Tbody,VT));    
    %
    %   Rotate the femur down, then shift to body frame, and apply body T.
    %   Notice, the femur angle is relative to the ankle frame. Femur will rotate from ankle frame rotation.
    %
    T=tmultt(Tbody,tmultt(t1r1(0,tibia_length,2),t1r1(femur_angle,0,1)));
    RenderFemur(T);
    RenderArc(t1r1(hipElevationDegrees,0,1),rangeDegrees);
    pause(1);    
end


%%
%   Calculate and plot the accel values for a surgical movement.
%
figure(2)
clf
title(['Simulated Accel VeraSense ',num2str(VeraSenseDegrees)]);
xlabel('Rotation in ankle frame. [degrees]');
ylabel('[G]');
axis([-rangeDegrees,+rangeDegrees,sin(-rangeDegrees*pi/180),+1.0]); hold on

figure(1)
RenderArc(t1r1(hipElevationDegrees,0,1),rangeDegrees);

%%
%   Move -+rangeDegrees.
%   The tilt rotation is about the ankle z axis. Not a rotation in the accel frame. 
%
accelI=eye(3,3);  % Each column is a unit vector. Gravity will show up as the x component of each unit vector.
femur_angle=-(180-(90-(tibiaElevationDegrees-hipElevationDegrees))-acos(tibia_length*sin((tibiaElevationDegrees-hipElevationDegrees)*pi/180)/femur_length)*180/pi);

N=305;  % Number of points along the arc to compute.
accelAll=zeros(N,3);
accelOriginal=zeros(N,3);   % Original points without VeraSense misorientation.

for i=1:N
    tiltDegrees=-rangeDegrees + 2*(i-1)/N*rangeDegrees;
    % 
    %   T transforms points in ankle frame to the accel frame.
    %   Tilt is rotation about ankle +z vector (which depends on the hipElevationDegees).
    %
    T=tmultt(t1r1(hipElevationDegrees,0,1),tmultt(t1r1(tiltDegrees,0,2),t1r1(tibiaElevationDegrees-hipElevationDegrees,0,1)));    
    %%
    %   To get accel signals we use inverse rotation of the transform.
    %    
    Tinv=[inv(T(:,1:3)),[0;0;0]];
    accelOriginal(i,:)=tmultv(Tinv,[1;0;0])';    
    %
    %   For accel values all we care about are the rotational terms (not translation) applied to the accel vector.
    %
%    T2=tmultt(T,[VT(:,1:3),[0;0;0]]);
%    accelAll(i,:)=T2(1,1:3); % First column contains the x values which corresponds to gravity.

    accelAll(i,:)= tmultv(tmultv(VT,Tinv), [1;0;0])';  % Measurement with tibia in position.

    plotMe=~mod(tiltDegrees,max(1,rangeDegrees/10));    
    if (plotMe)
        figure(1)
        %%
        %   Build up title with non-zero parameters.
        % Hip=',num2str(hipElevationDegrees)
        %title(['Geometry in accel frame.' ]);
        title(['Geometry in Accel frame. move=',num2str(tiltDegrees)]);
        RenderTibia(T);
        RenderVeraSense(tmultt(T,VT));    
        %   Notice we don't need femur values for calculations. Only for display.
        %   Rotate the femur down, then shift to body frame, and apply body T.
        %
        RenderFemur(tmultt(T,tmultt(t1r1(0,tibia_length,2),t1r1(femur_angle,0,1))));
    end
end
%%
%   Move back to zero y accel position.
%
tiltDegrees=0;
T=tmultt(t1r1(hipElevationDegrees,0,1),tmultt(t1r1(tiltDegrees,0,2),t1r1(tibiaElevationDegrees-hipElevationDegrees,0,1)));    
figure(1)
%   Build up title with non-zero parameters.
% Hip=',num2str(hipElevationDegrees)
%title(['Geometry in accel frame.' ]);
title(['Geometry in Accel frame. move=',num2str(tiltDegrees)]);
RenderTibia(T);
%%   Notice we don't need femur values for calculations. Only for display.
%   Rotate the femur down, then shift to body frame, and apply body T.
%
RenderFemur(tmultt(T,tmultt(t1r1(0,tibia_length,2),t1r1(femur_angle,0,1))));
RenderVeraSense(tmultt(T,VT));

unitAxis(tmultp(tmultt(T,t1r1(0,tibia_length,2)),[[0;0;0], eye(3,3)]),0.2);  % Original position without any VeraSense misadjustment.


%%
%   Plot simulated accel signals. (no simulated motion, just stationary at each position.
%
figure(2)
tiltDegrees=-rangeDegrees + 2*((1:N)-1)/N*rangeDegrees;

plot(tiltDegrees,accelOriginal(:,1),'r--'); hold on;
plot(tiltDegrees,accelOriginal(:,2),'g--');
plot(tiltDegrees,accelOriginal(:,3),'b--');

if (N<=100)
    plot(tiltDegrees,accelAll(:,1),'r+'); hold on
    plot(tiltDegrees,accelAll(:,2),'gx'); 
    plot(tiltDegrees,accelAll(:,3),'bx');
end
plot(tiltDegrees,accelAll(:,1),'r'); 
plot(tiltDegrees,accelAll(:,2),'g');
plot(tiltDegrees,accelAll(:,3),'b');

axis([-rangeDegrees,+rangeDegrees,-.2,0.8]);
grid on
legend('x','y','z');

%w=abs(accelAll(:,1)+1i*accelAll(:,3));
%plot(tiltDegrees,w,'k');

%tiltEst=atan(-accelAll(:,2)./w)*180/pi;
%plot(tiltDegrees,tiltEst,'m')
