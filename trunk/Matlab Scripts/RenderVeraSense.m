%%
%   Represent the VeraSense unit with a simple set of unit vectors at the end of the tibia for now.
%
%
function [ xyz ] = RenderVeraSense(T)
    persistent veraSenseHandles VeraSense;
    
    if isempty(veraSenseHandles)
        VeraSense=[[0;0;0], eye(3,3)];
        veraSenseHandles=unitAxis(tmultp(T,VeraSense),0.2);
    else
        veraSenseHandles=unitAxis(tmultp(T,VeraSense),0.2,veraSenseHandles);
    end
end

