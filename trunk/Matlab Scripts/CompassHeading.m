function [ heading ] = CompassHeading(accel,mag)
%CompassHeading This function returns the tilt compensated compass heading.
%  accel should be calibrated to 1G and in aircraft orientation frame.
%  mag should be normalized and in aircraft frame.

assert(2==nargin);
assert(min([3,1]==size(accel)));
assert(min([3,1]==size(mag)));

if (abs(1.0 - norm(accel)) < 0.1)
    %
    %  Find the pitch and roll angles from the accelerometer.
    %  From Eq 10 in AN3192 app note.
    %    
    yaw=0;
    if (1)
        pitch=asin(-accel(1));
        roll=asin(accel(2)/cos(pitch));
    else
        disp('CompassHeading() force pitch=0, roll=0');
        roll=0;
        pitch=0;
    end
    
    mag=mag/norm(mag);
    %%
    %   Compensate digital compass measurements for any pitch and roll.
    %
    if (1)
        Rr=[[1,0,0];[0,cos(roll),sin(roll)];[0,-sin(roll),cos(roll)]];
        Rp=[[cos(pitch),0,-sin(pitch)];[0,1,0];[sin(pitch),0,cos(pitch)]];
        Ry=[[cos(yaw),sin(yaw),0];[-sin(yaw),cos(yaw),0];[0,0,1]];
        
        M2=inv(Rp*Rr)*mag;
    else
        %
        %  Eq2 uses measurements (Xm=Xb, Ym=-Yb, Zm=-Zb).
        %  Eq11 use Xb.
        %
        M2(1)= mag(1)*cos(pitch)                                + mag(3)*sin(pitch);
        M2(2)= mag(1)*sin(pitch)*sin(roll) + mag(2)*cos(roll)   - mag(3)*sin(roll)*cos(pitch);
        M2(3)=-mag(1)*sin(pitch)*cos(roll) + mag(2)*sin(roll)   + mag(3)*cos(roll)*cos(pitch);  % Compute this only so we can compute norm().
    end

    if (abs(1.0 - norm(mag)) < 0.1)
        if (abs(1.0 - norm(M2)) < 0.1)
            %
            %   Matlab has two part arctan which uses sign to determine
            %   quadrant.
            %
            heading= atan2(M2(2),M2(1))*180/pi;
            if (heading <0)
                heading =heading+360;
            end

            disp(sprintf('%+4.1f Heading %+4.1f Pitch %+4.1f Roll.',heading,pitch*180/pi,roll*180/pi));
        else
            disp(['Pitch ',num2str(pitch*180/pi),' Roll ',num2str(roll*180/pi)]);
    [mag;norm(mag)]'
    [M2;norm(M2)]
            disp(['Pitch/roll error is present.',norm(M2)]);
            heading =0;
        end
    else
        [mag;norm(mag)]'
        [M2;norm(M2)]'
        disp(['External magnetic interference field is present. Norm is ',num2str(norm(mag))]);
        heading =0;
    end
else
    disp(['Linear or angular acceleration detected. ',norm(accel)]);
    heading =0;
end
