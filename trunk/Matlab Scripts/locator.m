%%
%   locator.m  
%   Given a set of measurements on the exterior surface of a sphere this function finds the center position (and radius).
%   Measurements are 3D position and 3D orientation vector.
%   
%   Solution minimizes the LS distance from a point to all the lines defined by the position and orientation.
%
function [center]=locator(position, unit_vector,misalign)

    if verLessThan('matlab', '7.10.0')
        %
        %   Previous versions don't support assert().
        %   I didn't check to see exactly when assert is supported.
        %
    else
        assert(3 == nargin);
        assert(3==size(position,1));    % position should be 3xL.
        assert(3==size(unit_vector,1)); % unit_vector should be 3xL.
        assert(1==min(size(position)==size(unit_vector)));
    end
    %
    %   LS solve for minimum distance of a point to all of the lines.
    %   Consider a cost function as the sum of squared distance from a point to a set of lines.
    %   Find the point x which minimizes the cost function.
    %
    L=size(position,2); % Number of lines specified by points and unit vectors.
    I=eye(3,3);

    R= L*I-unit_vector*unit_vector';
    %
    %   There is a more compact matrix calculation for P, but I just used explicit summation.
    %
    P=zeros(3,1);
    for (i=1:L)
        v=unit_vector(:,i);
        P=P+(I-v*v')*position(:,i);
    end
    center=pinv(R)*P;
    
    plot_inputs(position',unit_vector',center,misalign)    
end


%%
%   Plots the position and vector.
%   This will change to 3D later.
%s
function plot_inputs(position,unit_vector,center,misalign)

    if verLessThan('matlab', '7.10.0')
    else
        assert(4 == nargin);
        assert(3==size(center,1));     % center should be 3x1.
        assert(3==size(position,2));    % position should be Lx3.
        assert(3==size(unit_vector,2)); % unit_vector should be Lx3.
        assert(1==min(size(position)==size(unit_vector)));
    end

    %%  
    %   Plot center point.
    %
    figure(1)
    hold off
    plot(center(1),center(2),'rx')
    hold on
    plot(center(1),center(2),'mo')
    %%
    %   Plot the input position points and unit vectors in blue.  
    %
    plot(position(:,1),position(:,2),'+')   
    plot(position(:,1),position(:,2),'x')
    plot(position(:,1),position(:,2),'o')
    %
    %   Plot the unit_vector (orientation measurement).
    %
    quiver(position(:,1),position(:,2),unit_vector(:,1),unit_vector(:,2),'b','LineWidth',1);
    axis equal
    grid on
    title(sprintf('Solution using positions and angles, misalign %d degrees',misalign*180/pi))
    xlabel('X [m]')
    ylabel('Y [m]')
    %zlabel('Z [cm]')

    b=position-0.6*unit_vector; % Points on each extended line.
    for (row=1:size(position,1))
        %
        %   Plot lines extended from measured positions and orientations.
        %   Use parametric representation of line: line=point+t*unit_vector
        %
        plot([position(row,1),b(row,1)],[position(row,2),b(row,2)],'r');        
        if (1)
            %
            %   Plot normal. 
            %   Find t such that center+t*normal_vector == position+t*unit_vector.
            %
            u=unit_vector(row,:)';
            p=position(row,:)';
            u=unit_vector(row,:)';
            normal_vector=[[0,-1,0];[1,0,0];[0,0,1]]*u;

            t=pinv([u(1),-u(2),0;u(2),u(1),0;0,0,1]) * (center-p);
            normal_point=center-t(2)*normal_vector;
            plot([center(1,1),normal_point(1,1)],[center(2,1),normal_point(2,1)],'m');
        end
    end
end     % plot_inputs().
