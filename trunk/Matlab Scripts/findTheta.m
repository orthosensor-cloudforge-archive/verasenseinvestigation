%%  
%   Find the best starting position. Flexion and Circumduction?
%   This demonstrates that you can not make up for VeraSense misadjustments with just the circumduction and that it is not a 1:1 degree offset.
%

if (1)
    VeraSenseDegrees=[ 5.0, 0.0, 0.0];  % Rotation of the VeraSense unit relative to the tibia.
    VT=tmultt(t1r1(-VeraSenseDegrees(3),0,2),tmultt(t1r1(-VeraSenseDegrees(2),0,1),t1r1(-VeraSenseDegrees(1),0,0)));  % VeraSense miss adjustment.
%    VT=tmultt(t1r1(0,tibia_length,2),VT);   % Slide to knee.
else
    SurgicalMovement
end

%   T=tmultt(t1r1(hipElevationDegrees,0,1),tmultt(t1r1(tiltDegrees,0,2),t1r1(tibiaElevationDegrees-hipElevationDegrees,0,1)));    
%   accelOriginal(i,:)=T(1,1:3);    % Accel values are the x components of the transform.        
%   accelAll(i,:)= tmultv(tmultt([VT(:,1:3),[0;0;0]],tmultt(t1r1(-tibiaElevationDegrees,0,1),t1r1(-tiltDegrees,0,2))), [1;0;0])';  % Measurement with tibia in position.


%startGravity=tmultv(tmultt(t1r1(-45,0,1),t1r1(-tiltDegrees,0,2)), [1;0;0])';  
%measure=tmultv(VT,startGravity);

%%
%   Surgeon is trying to line up 45 degrees flexion and 0 tilt indicated by the VeraSense unit.
%   He can control the "tilt" and flexion.
%
opt=[1/sqrt(2); 0; 1/sqrt(2)];  % This is what we are trying to achieve.


tilt=-5: 0.01: +5;
extension=44.5: 0.01: 45.5;

e=zeros(length(tilt),length(extension));
for r=1:length(tilt)
    for c=1:length(extension)
        
        accel=tmultv(tmultt(t1r1(-extension(c),0,1),t1r1(-tilt(r),0,2)), [1;0;0]); 
        measure=tmultv(VT,accel);
        
        e(r,c)=norm(opt-measure);
    end
end

%%
[i,j]=find(e <=min(min(e)));
fprintf('Best match [%2.1f,%2.1f,%2.1f] surgical start position is tilt=%4.2f extension=%4.2f error=%f.\n',VeraSenseDegrees,tilt(i),extension(j),e(i,j));

%%
figure(99)
surf(extension, tilt,  e)
title('Initial lineup')
xlabel('extension');
ylabel('tilt');
