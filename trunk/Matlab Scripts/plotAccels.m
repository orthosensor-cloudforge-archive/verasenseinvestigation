%
%   Plot accelAll, accelOriginal over tiltDegrees.
%

%%
%   Calculate and plot the accel values for a surgical movement.
%
figure(2)
clf
title(['Simulated Accel VeraSense ',num2str(VeraSenseDegrees)]);
xlabel('Rotation in ankle frame. [degrees]');
ylabel('[G]');
%axis([-rangeDegrees,+rangeDegrees,sin(-rangeDegrees*pi/180),+1.0]); hold on
%%
%   Plot simulated accel signals. (no simulated motion, just stationary at each position.
%
figure(2)
%tiltDegrees=-rangeDegrees + 2*((1:N)-1)/N*rangeDegrees;

plot(tiltDegrees,accelOriginal(:,1),'r--'); hold on;
plot(tiltDegrees,accelOriginal(:,2),'g--');
plot(tiltDegrees,accelOriginal(:,3),'b--');

if (N<=100)
    plot(tiltDegrees,accelAll(:,1),'r+'); hold on
    plot(tiltDegrees,accelAll(:,2),'gx'); 
    plot(tiltDegrees,accelAll(:,3),'bx');
end
plot(tiltDegrees,accelAll(:,1),'r'); 
plot(tiltDegrees,accelAll(:,2),'g');
plot(tiltDegrees,accelAll(:,3),'b');

%axis([-rangeDegrees,+rangeDegrees,-.2,0.8]);
grid on
legend('x','y','z');
