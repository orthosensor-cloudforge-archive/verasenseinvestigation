%
%   If the VeraSense unit is perfectly aligned and tibia is at 45 degrees, x=z=cos(angle)/sqrt(2), y=sin(angle)
%
VeraSenseDegrees=[0.0, 0.0, 0.0];  % Rotation of the VeraSense unit relative to the tibia.
tibiaElevationDegrees=50;                       % Angle of tibia relative to plane normal to gravity.
hipElevationDegrees=+0.0;                       % Angle of elevation of hip relative to ankle.
rangeDegrees= 5;                                % Surgical movement will go from -+rangeDegrees. (As indicated by accel.)
tibia_length=0;
femur_length=0;
%
%   RotZ(RotY(RotX)))
%
VT=tmultt(t1r1(-VeraSenseDegrees(3),0,2),tmultt(t1r1(-VeraSenseDegrees(2),0,1),t1r1(-VeraSenseDegrees(1),0,0)));  % VeraSense misadjustment.
%VT=tmultt(t1r1(0,tibia_length,2),VT);   % Slide to knee.


%%
%   Move -+rangeDegrees.
%   The tilt rotation is about the ankle z axis. Not a rotation in the accel frame. 
%
accelI=eye(3,3);  % Each column is a unit vector. Gravity will show up as the x component of each unit vector.
femur_angle=-(180-(90-(tibiaElevationDegrees-hipElevationDegrees))-acos(tibia_length*sin((tibiaElevationDegrees-hipElevationDegrees)*pi/180)/femur_length)*180/pi);

N=305;  % Number of points along the arc to compute.
accelAll=zeros(N,3);
accelOriginal=zeros(N,3);   % Original points without VeraSense misorientation.

for i=1:N
    tiltDegrees=-rangeDegrees + 2*(i-1)/N*rangeDegrees;
    % 
    %   T transforms points in ankle frame to the accel frame.
    %   Tilt is rotation about ankle +z vector (which depends on the hipElevationDegees).
    %
    T=tmultt(t1r1(hipElevationDegrees,0,1),tmultt(t1r1(tiltDegrees,0,2),t1r1(tibiaElevationDegrees-hipElevationDegrees,0,1)));    
    %%
    %   To get accel signals we use inverse rotation of the transform.
    %    
    Tinv=[inv(T(:,1:3)),[0;0;0]];
    accelOriginal(i,:)=tmultv(Tinv,[1;0;0])';    
    %
    %   For accel values all we care about are the rotational terms (not translation) applied to the accel vector.
    %
%    T2=tmultt(T,[VT(:,1:3),[0;0;0]]);
%    accelAll(i,:)=T2(1,1:3); % First column contains the x values which corresponds to gravity.

    accelAll(i,:)= tmultv(tmultv(VT,Tinv), [1;0;0])';  % Measurement with tibia in position.
end
accelAll

figure(3)
if (1)
    tiltDegrees=-rangeDegrees+2/N*rangeDegrees:2/N*rangeDegrees:rangeDegrees;
    
    clf
    title('Perfect motion'); hold on; grid on; % axis([min(tiltDegrees),max(tiltDegrees),-.25,0.75]);
    plot(tiltDegrees,accelOriginal(:,1),'r-.'); 
    plot(tiltDegrees,accelOriginal(:,2),'g-.');
    plot(tiltDegrees,accelOriginal(:,3),'b-.');

    title('Inverse Kinematics Recovery');
    plot(tiltDegrees,accelAll(:,1),'r');
    plot(tiltDegrees,accelAll(:,2),'g');
    plot(tiltDegrees,accelAll(:,3),'b');
else
    title('Inverse Kinematics Recovery');
    plot(accelAll(:,1),'r'); hold on
    plot(accelAll(:,2),'g');
    plot(accelAll(:,3),'b');
    grid on
end

%%
%   This works, but rank is only 2. Can not uniquely determine all three angles.
%
b=accelOriginal;


VeraSenseRotation=lscov(accelAll,b)
recover=transpose(b*pinv(VeraSenseRotation));

figure(99), clf, title('Recovered????'); hold on; grid on;
plot(tiltDegrees,recover(1,:),'r+');
plot(tiltDegrees,recover(2,:),'g+');
plot(tiltDegrees,recover(3,:),'b+');



break;
%%
%   Test case.
%
N=12;
tiltDegrees=-rangeDegrees+2/N*rangeDegrees:2/N*rangeDegrees:rangeDegrees;
tilt=tiltDegrees*pi/180;
hipElevationDegrees=+0.0;                       % Angle of elevation of hip relative to ankle.
rangeDegrees= 5;                                % Surgical movement will go from -+rangeDegrees. (As indicated by accel.)

VeraSenseDegrees=[1.0, 1.0, 1.0];  % Rotation of the VeraSense unit relative to the tibia.
%
%   RotZ(RotY(RotX)))
%
VT=tmultt(t1r1(VeraSenseDegrees(3),0,2),tmultt(t1r1(VeraSenseDegrees(2),0,1),t1r1(VeraSenseDegrees(1),0,0)));  % VeraSense misadjustment.
VT=tmultt(t1r1(0,tibia_length,2),VT);   % Slide to knee. Not necessary except for graphics.

tibiaElevationDegrees=45;
A1=[cos(tibiaElevationDegrees*pi/180)*cos(tilt);sin(tilt);cos((90-tibiaElevationDegrees)*pi/180)*cos(tilt)]'
b1=A1;
A1=tmultp(VT,A1')';

tibiaElevationDegrees=50;
A2=[cos(tibiaElevationDegrees*pi/180)*cos(tilt);sin(tilt);cos((90-tibiaElevationDegrees)*pi/180)*cos(tilt)]'
b2=A2;
A2=tmultp(VT,A2')';

A=[A1; A2];
b=[b1; b2];
VeraSenseRotation=lscov(A,b)

%
%   Should equal identity.
%
VT(:,1:3)*pinv(VeraSenseRotation)
%
%   Solve for angles in ZYX order.
%
[x2,y2,z2] = decompose_rotation(VeraSenseRotation);
[x2,y2,z2]*180/pi

err=norm(VeraSenseDegrees-[x2,y2,z2]*180/pi)

%%

%   See if I can at least rotate accelAll measurements back to perfect alignment. This works. But I am still up against non-unique solutions (x=z?)
%   
if (0)
    VTinv=[inv(VT(:,1:3)),[0;0;0]];
    c=tmultp(VTinv,accelAll');acc

    plot(tiltDegrees,c(1,:),'rx');
    plot(tiltDegrees,c(2,:),'gx');
    plot(tiltDegrees,c(3,:),'bx');
end


if (0)
    %
    %   Test b estimate.
    %
    Tx=t1r1(-pi/2,0,0);
    Tyx=tmultt(t1r1(-0,0,1),Tx);
    Tzyx=tmultt(t1r1(+1,0,2),Tyx);  % VeraSense misadjustment.
    Vinv=[inv(Tzyx(:,1:3)),[0;0;0]];

    z = tmultp(Vinv,accelAll')';  % This is the measurement corrected for what we are guessing the VeraSense misalignment is.
    %
    %   Use Vinv and measurement to estimate the anatomical tilt at each measurement point with the assumed VeraSense misadjustment.
    %
    tiltEst=atan2(-z(:,2),sqrt(z(:,1).^2 + z(:,3).^2))*180/pi
    [tiltDegrees', tiltEst, tiltDegrees'-tiltEst]

    b=tmultp(t1r1(-tibiaElevationDegrees,0,1),[cos(-tiltEst*pi/180), sin(-tiltEst*pi/180), zeros(size(tiltEst))]')';
    e = z-b;
end
