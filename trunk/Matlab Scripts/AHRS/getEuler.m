%%
%   Returns the Euler angles in degrees as defined with the Aerospace coordinates.
%   See Sebastian O.H. Madwick report 
%   "An efficient orientation filter for inertial and intertial/magnetic sensor arrays" Chapter 2 Quaternion representation.
%
%void FreeIMU::getEuler(float * angles) {
%  float q[4]; // quaternion
%  getQ(q);
%  angles[0] = atan2(2 * q[1] * q[2] - 2 * q[0] * q[3], 2 * q[0]*q[0] + 2 * q[1] * q[1] - 1) * 180/M_PI; // psi
%  angles[1] = -asin(2 * q[1] * q[3] + 2 * q[0] * q[2]) * 180/M_PI; // theta
%  angles[2] = atan2(2 * q[2] * q[3] - 2 * q[0] * q[1], 2 * q[0] * q[0] + 2 * q[3] * q[3] - 1) * 180/M_PI; // phi
%}
function [angles]= getEuler(q)
    
    assert(1==nargin);
    assert(min(size(q)==[4,1]));
    
    angles(1,1) = atan2(2 * q(2) * q(3) - 2 * q(1) * q(4), 2 * q(1)*q(1) + 2 * q(2) * q(2) - 1) * (180/pi); %  psi
    angles(2,1) = -asin(2 * q(2) * q(4) + 2 * q(1) * q(3)                                     ) * (180/pi); %  theta
    angles(3,1) = atan2(2 * q(3) * q(4) - 2 * q(1) * q(2), 2 * q(1)*q(1) + 2 * q(4) * q(4) - 1) * (180/pi); %  phi
end
