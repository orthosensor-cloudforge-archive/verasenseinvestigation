%%=====================================================================================================
% AHRS.c
% S.O.H. Madgwick
% 25th August 2010
% =====================================================================================================
% Description:
%
% Quaternion implementation of the 'DCM filter' [Mayhony et al].  Incorporates the magnetic distortion
% compensation algorithms from my filter [Madgwick] which eliminates the need for a reference
% direction of flux (bx bz) to be predefined and limits the effect of magnetic distortions to yaw
% axis only.
%
% User must define 'halfT' as the (sample period / 2), and the filter gains 'Kp' and 'Ki'.
%
%  Global variables 'q0', 'q1', 'q2', 'q3' are the quaternion elements representing the estimated
%  orientation.  See my report for an overview of the use of quaternions in this application.
% 
%  User must call 'AHRSupdate()' every sample period and 
%   accel[3] [units don't matter since normalized]
%   gyro[3]  [radians/sec]
%   magn[3]  [units don't matter since normalized]
%   halfT    half the sample period [sec].
%   quaternion Input/Output variable is the quaternion vector.
% =====================================================================================================
function [quaternion] = AHRSUpdate(accel,gyro,magn,halfT,quaternion)
    assert(5 == nargin);                % Check number of arguments to function.
    assert(min(size(accel)==[3,1]));    % Check size of input.
    assert(min(size(gyro )==[3,1]));    % Check size of input.
    assert(min(size(magn )==[3,1]));    % Check size of input.
    assert(min(size(halfT)==[1,1]));    % Check size of input.
    assert(min(size(quaternion)==[4,1]));    % Check size of input.
    
    persistent exInt;
    persistent eyInt;
    persistent ezInt;
    
    if (isempty(exInt) || ~isfinite(exInt))
        exInt=0.0;
        eyInt=0.0;
        ezInt=0.0;
    end
        
    Kp=2.0;     % Proportional gain governs rate of convergence to accelerometer/magnetometer.
    Ki=0.005;   % Integral gain governs rate of convergence of gyroscope biases.

    %%
    %   Precompute some auxiliary variables to reduce number of repeated operations.
    %   Matlab probably calculates q*q' faster than individual operations.  This should be tested and timed.
    %
    Q=quaternion*quaternion';
    q0q0 = Q(1,1);    % Matlab is indexed starting with 1.  Probably faster to replace all these variables with direct ref to Q(r,c).
    q0q1 = Q(1,2);
    q0q2 = Q(1,3);
    q0q3 = Q(1,4);
    q1q1 = Q(2,2);
    q1q2 = Q(2,3);
    q1q3 = Q(2,4);
    q2q2 = Q(3,3);   
    q2q3 = Q(3,4);
    q3q3 = Q(4,4);          
    %%
    %  Normalise the accel and magn measurements.
    %
    accel = accel / norm(accel);
    magn  = magn  / norm(magn);
    %%
    %   To simplify porting, I convert to original algorithm names here.  Matlab would be faster if this were vectorized.
    %
    ax=accel(1);
    ay=accel(2);
    az=accel(3);

    mx=magn(1);
    my=magn(2);
    mz=magn(3);

    gx=gyro(1);
    gy=gyro(2);
    gz=gyro(3);
    
    q0=quaternion(1);
    q1=quaternion(2);
    q2=quaternion(3);
    q3=quaternion(4);
    %%
    %  Compute reference direction of flux.
    %
    hx = 2*mx*(0.5 - q2q2 - q3q3) + 2*my*(q1q2 - q0q3) + 2*mz*(q1q3 + q0q2);
    hy = 2*mx*(q1q2 + q0q3) + 2*my*(0.5 - q1q1 - q3q3) + 2*mz*(q2q3 - q0q1);
    hz = 2*mx*(q1q3 - q0q2) + 2*my*(q2q3 + q0q1) + 2*mz*(0.5 - q1q1 - q2q2);             
    bx = sqrt((hx*hx) + (hy*hy));   % This will always be non-negative.
    bz = hz;     
    %%
    %  Estimated direction of gravity and flux (v and w).
    %
    vx = 2*(q1q3 - q0q2);
    vy = 2*(q0q1 + q2q3);
    vz = q0q0 - q1q1 - q2q2 + q3q3;
    
    wx = 2*bx*(0.5 - q2q2 - q3q3) + 2*bz*(q1q3 - q0q2);
    wy = 2*bx*(q1q2 - q0q3) + 2*bz*(q0q1 + q2q3);
    wz = 2*bx*(q0q2 + q1q3) + 2*bz*(0.5 - q1q1 - q2q2);  
    %%
    %  Error is sum of cross product between reference direction of fields and direction measured by sensors.
    %
    ex = (ay*vz - az*vy) + (my*wz - mz*wy);
    ey = (az*vx - ax*vz) + (mz*wx - mx*wz);
    ez = (ax*vy - ay*vx) + (mx*wy - my*wx);
    %%
    %  Integral error scaled integral gain.
    %
    exInt = exInt + ex*Ki;
    eyInt = eyInt + ey*Ki;
    ezInt = ezInt + ez*Ki;
    %%
    %  Adjusted gyroscope measurements.
    %
    gx = gx + Kp*ex + exInt;
    gy = gy + Kp*ey + eyInt;
    gz = gz + Kp*ez + ezInt;
    %%
    %  Integrate quaternion rate and normalise.
    %
    integrated_q(1,1) = (-q1*gx - q2*gy - q3*gz)*halfT;
    integrated_q(2,1) = ( q0*gx + q2*gz - q3*gy)*halfT;
    integrated_q(3,1) = ( q0*gy - q1*gz + q3*gx)*halfT;
    integrated_q(4,1) = ( q0*gz + q1*gy - q2*gx)*halfT;  

    quaternion =quaternion + integrated_q;
    %%
    %  Normalise quaternion.
    %
    quaternion = quaternion / norm(quaternion);
end % function AHRSUpdate().