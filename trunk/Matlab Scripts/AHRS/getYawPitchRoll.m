%%  
%   Return a row vector of Yaw,Pitch,Roll scaled to degrees for a given quaternion.
%   Uses aircraft convention for Yaw, Pitch, Row.
%   Input is 4x1 quaternion.
%
function [ypr]=getYawPitchRoll(q)
    %
    %  Estimated gravity direction.
    %
    assert(1==nargin);
    assert(min([4,1]==size(q)));

    q0=q(1);
    q1=q(2);
    q2=q(3);
    q3=q(4);
    %
    %   Find the gravity direction.
    %
    gx = 2 * (q1*q3 - q0*q2);
    gy = 2 * (q0*q1 + q2*q3);
    gz = q0*q0 - q1*q1 - q2*q2 + q3*q3;

    ypr(1) = atan2(2 * q1 * q2 - 2 * q0 * q3, 2 * q0*q0 + 2 * q1 * q1 - 1) * 180/pi;
    ypr(2) = atan(gx / sqrt(gy*gy + gz*gz))  * 180/pi;
    ypr(3) = atan(gy / sqrt(gx*gx + gz*gz))  * 180/pi;
    %
    %   Return values 0..360 degrees.
    %
    r=find(ypr<0.0);        % Find index of elements out of range.
    ypr(r)=ypr(r)+360.0;    % Keep in range of 0 to 360 degrees.
end
