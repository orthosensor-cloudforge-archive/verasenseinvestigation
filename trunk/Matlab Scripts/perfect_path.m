%%
%   Generate accel and gyro data for a path on a sphere.
%   This data can be used to verify processing accel and orientation into position.
%   Notice a perfect circular path on the sphere does not uniquely determine the size of the sphere.
%
T=5.0;            % Period of the rotation measurement.
Ts=1/200;         % Sample period. 
t_samples=[0:Ts:T-Ts];  % sampled every 10msec.

HipJointModel;  % Geometry of hip joint.

femur_length
rotation_angle=pi/8;
rotation_degrees=rotation_angle*180/pi

phase=t_samples*2*pi/T;  % Complete revolution in T seconds at constant angular velocity.

if (0)
    %%
    %   Simple linear acceleration to test integration algorithm (including orientation).
    %   The acceleration specified below will result in exactly 0.9 meters of movement in one direction.
    %
    T=1.0;                  % Period of the rotation measurement.
    Ts=0.010;               % 10 msec sample period. (100 hz)
    t_samples=[0:Ts:T-Ts];  % sampled every 10msec.
    
    accel=[ 10.0*ones(1,12),zeros(1,size(t_samples,2)-24),-10.0*ones(1,12);           
            zeros(size(t_samples));           
            zeros(size(t_samples))
           ]';
    velocity=[0,0,0];   % Only needed for initial conditions.
    position=[0,0,0];   % Only needed for initial conditions.
    %
    %   Add in gravity.
    %
    if (1)  
        accel(:,3)=accel(:,3)+9.81;
    else
        disp('Perfect path is ignoring gravity.');
    end
    %
    %   Scale to 1G.
    %
    accel=accel/9.81;
    if (0)
        %
        %   Add in pitch and roll.
        %
        YPR=[zeros(size(t_samples));   % Yaw.
            90*ones(size(t_samples));  % Pitch.
            90*ones(size(t_samples)); ]';  % Roll. 
    else
        %
        %   No rotation.
        %
        YPR=zeros(size(accel));   
    end
    return
end

%%
%   Constant angular velocity rotation.  
%
x=cos(rotation_angle)*femur_length*ones(size(phase));  % X distance won't change for this rotation.
y=sin(rotation_angle)*femur_length*cos(phase);
z=sin(rotation_angle)*femur_length*sin(phase);

figure(20)
plot3(x,y,z)
title('Position Path');
xlabel('x[m]'),ylabel('y[m]'),zlabel('z[m]');
axis equal

%
%   Simple enough to differentiate to find velocity and acceleration.
%   These arrays will all be Nx3 where N=T/Ts;
%
position=[x;y;z]';
velocity=[zeros(size(phase)); ...
          sin(rotation_angle)*femur_length*(-sin(phase)*2*pi/T);
          sin(rotation_angle)*femur_length*(+cos(phase)*2*pi/T)]';
accel=   [zeros(size(phase));...      
          sin(rotation_angle)*femur_length*(-cos(phase)*(2*pi/T)^2);
          sin(rotation_angle)*femur_length*(-sin(phase)*(2*pi/T)^2)]';

if (0)
    YPR=[rotation_degrees*cos(phase);
    rotation_degrees*sin(phase);
    zeros(size(phase))]';
    %
    %   Add in gravity.  Gravity gets projected onto x,z IMU coordinates.
    %
    if (1)
        accel=accel+9.81*[-sin(rotation_angle)*sin(phase);
                       zeros(size(phase));
                       sqrt(1-sin(rotation_angle)*sin(rotation_angle)*sin(phase).*sin(phase))]';
    else
       disp('Perfect path is ignoring gravity.');
    end
else
    disp('Orientation zero out');
    YPR=zeros(size(accel));
    accel(:,3)=accel(:,3)+9.81;
end
%
%   Scale to 1G.
%
accel=accel/9.81;

if (1)
    %
    %   Plot the velocity and acceleration.
    %
    figure(21)
    clf;
    subplot(3,1,1);
    plot(t_samples,accel(:,1),'r'); hold on
    plot(t_samples,accel(:,2),'g');
    plot(t_samples,accel(:,3),'b');
    title('Perfect Acceleration (frame 0)');ylabel('m/s^2')
    grid on

    subplot(3,1,2)
    plot(t_samples,velocity(:,1),'r'); hold on
    plot(t_samples,velocity(:,2),'g');
    plot(t_samples,velocity(:,3),'b');
    title('Perfect Velocity (frame 0)');ylabel('m/s')
    grid on

    subplot(3,1,3)
    plot(t_samples,position(:,1),'r'); hold on
    plot(t_samples,position(:,2),'g');
    plot(t_samples,position(:,3),'b');
    title('Perfect Position (frame 0)');xlabel('sample'),ylabel('m')
    grid on
end
if (0)
    %%
    %   Using just the position we can use differences to estimate the velocity and acceleration.
    %   Plot the error using this method.
    %   This shows an error off O(1e-6) for velocity and acceleration just from finite differences.  (gets better with faster sample rate.)
    %
    figure(22)
    clf
    velocity_diff=diff(position)/Ts;
    accel_diff=diff(velocity_diff)/Ts;
    plot(t_samples(1:size(t,2)-1),sum(velocity_diff.*velocity_diff) - sum(velocity(:,1:size(t,2)-1).*velocity(:,1:size(t,2)-1)),'y');
    hold on
    title('Differentiation Error.');xlabel('t[sec]'),ylabel('error')
    plot(t_samples(1:size(t,2)-2),sum(accel_diff   .*accel_diff)  - sum(accel(:,1:size(t,2)-2).*accel(:,1:size(t,2)-2))      ,'c');
end

figure(22)
clf
plot3(position(:,1),position(:,2),position(:,3),'k+');   % Plot the position points.
hold on
for (i=1:size(position,1))
    plot3([0,position(i,1)],[0,position(i,2)],[0,position(i,3)])
    if (1==i)
        title('Perfect path'); xlabel('x[m]'),ylabel('y[m]'),zlabel('z[m]')
        hold on  
        axis([0,.5,-.2,.2,-.2,+.2]);
        view(35,8);
        grid on
    end
%    disp(sprintf('Yaw %6.2f Pitch %6.2f Roll %6.2f',YPR(i,:)));
%    disp(sprintf('Gravity %6.5f %6.5f %6.5f',-sin(rotation_angle)*sin(phase(i)),0,sqrt(1-sin(rotation_angle)*sin(rotation_angle)*sin(phase(i))*sin(phase(i)))));  % Gravity.
%    pause(0.010);            
end
