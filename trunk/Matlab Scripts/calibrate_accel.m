%
%   This script implements the acceleration calibration method detailed in AN3192
%   http://www.st.com/stonline/products/literature/an/17353.pdf
%
%%
%   Reads serial stream from Aruduino 9DOF board or files from the VeraSense RxTxUtility, displays the values, and returns the calibration matrix.
%

port=9;
disp('OrthoSensor VeraSense Accel calibration');

positions=[
    [+1,0,0];
    [-1,0,0];
    [ 0,+1,0];
    [ 0,-1,0];
    [ 0,0,+1];
    [ 0,0,-1];];

Y=[];
w=[];
accel_all=[];

N=200;   % Number of measurements at each position.
%
%   Collect measurements at each position.
%
for p=1:size(positions,1 )
    %
    %   Known positions.
    %
    Y=[Y; ones(N,1)*positions(p,:)];
    %
    %   Measurements.
    %
    if (0)
        %
        %   Simulate measurements for test.
        %
        A=256*positions(p,:);
        accel=ones(N,1)*[A,1]+0.1*rand(N,4);              
    elseif (0)
        disp(['Rotate IMU to position [',num2str(positions(p,:)),'].'])
        disp('Type any key to continue');
        pause
        [accel,gyro,magn,t]=collect_9DOM(port,N);
    elseif (1)
        %
        %   Use values from Tim's LabView .csv.
        %
        if (1 == p)
            load VeraSenseTim.csv
        end
        accel=VeraSenseTim(1:3,N*(p-1)+1:N*p)';
    else 
        %
        %   Use files from VeraSense RxTxUtility.
        %
        load(['RxTxData/Position',num2str(p),'.txt'])
         %%
        accel=eval(['Position',num2str(p),'(:,1:3)']);
        disp('Press any key to continue to next set of data.');        pause
    end
    accel_all=[accel_all; accel];   % Concatenate the positions together.
    w=[w;accel,ones(N,1)];    
    %
    %   Plot raw counts.
    %
    figure(1)
    clf
    plot(accel(:,1),'r'); hold on
    plot(accel(:,2),'g'); 
    plot(accel(:,3),'b'); 
    title(['Raw Counts Position ',num2str(p)]);
    ylabel('AtoD');
    xlabel('sample')
    ticks=0:8192:65536;
    set(gca,'YTick',ticks)
    set(gca,'YTickLabel',num2str(ticks'))
    axis([0,size(accel,1),0,2^16]);
    grid on
    
    pause(1);
end

%%
%   Calculate the three single axis calibrations.
%   This only works if the x,y,z are defined as for the yellow device.
%
if (0)
    singleAxisCalibrate
end

%%
%   LS solution for accel calibration parameters.
%   This will calibrate gain, offset, and rotations. (Notice this scales to +-1G.)
%
accel_calibration=lscov(w,Y)';
%%
%   Replace value in the file.
%
save('accel_calibration.mat','accel_calibration');
%%
%   Use the full 4x3 calibration matrix.
%
accel=[accel_all,ones(size(accel_all,1),1)] * accel_calibration';

%%
%   Plot values corrected with full calibrations.
%
figure(4)
clf
plotColor=['r','g','b'];
for a=1:3
    r1=[(2*(a-1))*N+1:(2*a-1)*N]; % range for +1 values.
    r2=[(2*a-1)  *N+1:(2*a  )*N];  % range for -1 values.
    
    plot(accel(r1,1),'r'), hold on
    plot(accel(r1,2),'g')
    plot(accel(r1,3),'b')
    
    plot(accel(r2,1),'r')
    plot(accel(r2,2),'g')
    plot(accel(r2,3),'b') 
end  
grid on
title('Calibrated.')
ylabel('[g]');
xlabel('sample');
axis([0,N,-1.1,+1.1]);

figure(5)
clf
plot3([-1,+1],[ 0, 0],[ 0, 0],'k'), hold on
plot3([ 0, 0],[-1,+1],[ 0, 0],'k')
plot3([ 0, 0],[ 0, 0],[-1,+1],'k')
for a=1:3
    r1=[(2*(a-1))*N+1:(2*a-1)*N]; % range for +1 values.
    r2=[(2*a-1)  *N+1:(2*a  )*N];  % range for -1 values.
    %
    %   Plot the mean of the +-1G.
    %
    v=mean(accel(r1,:));
    plot3([0,v(1)],[0,v(2)],[0,v(3)],plotColor(a))
    v=mean(accel(r2,:));
    plot3([0,v(1)],[0,v(2)],[0,v(3)],plotColor(a))
    
    plot3(accel(r1,1),accel(r1,2),accel(r1,3),[plotColor(a),'.']) % Plot clusters of points.
    plot3(accel(r2,1),accel(r2,2),accel(r2,3),[plotColor(a),'.'])
end  
grid on
title('Calibrated.')
xlabel('[Gx]');
ylabel('[Gy]');
zlabel('[Gz]');
axis([-1.1,+1.1,-1.1,+1.1,-1.1,+1.1],'equal');


%%
%   We want the numbers in the calibration matrix to be similar to each other in magnitude to avoid fixed point representation problems.
%   The top 3x3 diagonal values are the axis gains which will be approx 1.
%   The top 3x3 off diagonal values are the rotation terms (cos()*sin()) << 1 for our system which should be close to aligned.
%   The bottom 1x3 row contains the offset terms. This will be in AtoD counts and can be quite large relative to 1.
%
AtoD1G=24080;       % Should come from hardware AtoD design.
offsetScale=2^15    % Scale the dc offsets (last row of calibration matrix) to keep elements within Qm_n.
m=1;                % Number of bits for integer portion of the number.
b=24;               % Number of bits stored per value in the device.
n=b-1-m;            % Number of bits for the fractional portion of the number.
%
% xTest [1200x4] contains the measured values. (Six sets of 200x4) Last column is constant to pick up the dc offset.
% yTest [1200x3] contains the desired/known values.
%
yTest=Y*AtoD1G + 2^15;                                      % Scale the desired/known to match the scaling of the raw values.
xTest=[accel_all,  offsetScale*ones(length(accel_all),1)];  % Bake in the dc offset scale.
%
%   Use basic matrix operations to compute the calibration matrix.
%
calP=(xTest'*xTest)\xTest'*yTest
%
%   Test to make sure all values fit within Q2_22.
%
calTest=calP;
if (max(max(calTest)) > (2^m-2^-n)) || (min(min(calTest)) < -2^m)
    disp(['Calibration matrix does not fit within Q',num2str(m),'_',num2str(n),'.']);
    assert(0)
else
    %%
    %   Convert to Q1.22 value for storage into VeraSense.
    %
    calPQ1_22=round(calTest * 2^n)  % This is where the fixed point truncation comes in.
end

%%
%   Evaluate any error from direct calulation or fixed point representation.
%
cal=lscov(xTest,yTest)
calTest=cal;
if (max(max(calTest)) > (2^m-2^-n)) || (min(min(calTest)) < -2^m)
    disp(['Calibration matrix does not fit within Q',num2str(m),'_',num2str(n),'.']);
    assert(0)
else
    calQ1_22=round(calTest * 2^n);  % This is where the fixed point truncation comes in.
end

conditionNumber = cond(xTest)

calRecovered = calQ1_22  * 2^-n;                       % Convert Qm_n to floating point.
calRecoveredP= calPQ1_22 * 2^-n;                       % Convert Qm_n to floating point.

d=max(xTest*(cal-calRecoveredP))             % difference from lscov
d=max(xTest*(calRecovered-calRecoveredP))    % difference from lscov and fixed point.
