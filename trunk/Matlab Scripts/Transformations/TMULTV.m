%
%   Jerry J. Trantow
%
%   This function multiplies the rotational portion of a transformation matrix times
%   a vector. Does not include the translation terms.
%
function [Va]=tmultv(Tab,Vb)
    
    if (min(size(Tab)==[3 4]) && min(size(Vb,1)==3))
        Va=Tab(:,1:3)*Vb;
    else
        disp('tmultv() is for use with a 3x4 and 3xn vectors.');
        Va=[];
    end