function T = rotationT(radians,p)
%
% Returns the 3x4 transformation for a rotation of radians about the vector from the origin to the point defined by p.
%
assert(nargin == 2);

T=[rotationmat3D(radians,p),zeros(3,1)]
