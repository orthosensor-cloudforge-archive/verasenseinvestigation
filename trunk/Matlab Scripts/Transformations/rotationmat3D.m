function R = rotationmat3D(radians,Axis)
%function R= rotationmat3D(radians,Axis)
%
% creates a rotation matrix such that R * x 
% operates on x by rotating x around the origin r radians around line
% connecting the origin to the point "Axis"
%
assert(nargin == 2);

% useful intermediates
L = norm(Axis);
if (L < eps)
   error('axis direction must be non-zero vector');
else
    Axis = Axis / L;
end

u = Axis(1);
v = Axis(2);
w = Axis(3);
u2 = u^2;
v2 = v^2;
w2 = w^2;

c = cos(radians);
s = sin(radians);
%
% Preallocate and fill with nan to catch unassigned values.
%
R = nan(3);
% fill the matrix.
R(1,1) =  u2 + (v2 + w2)*c;
R(1,2) = u*v*(1-c) - w*s;
R(1,3) = u*w*(1-c) + v*s;
R(2,1) = u*v*(1-c) + w*s;
R(2,2) = v2 + (u2+w2)*c;
R(2,3) = v*w*(1-c) - u*s;
R(3,1) = u*w*(1-c) - v*s;
R(3,2) = v*w*(1-c)+u*s;
R(3,3) = w2 + (u2+v2)*c;