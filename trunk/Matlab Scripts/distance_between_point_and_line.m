
%%
%   Distance from a point x in 3D space to a line define by a point p and unit vector v.
%
function [d]=distance_between_point_and_line(x,p,v)

    if verLessThan('matlab', '7.10.0')

    else
        assert(3==nargin);
        assert(3==size(x,1)); % position should be 3xL.
        assert(3==size(p,1)); % unit_vector should be 3xL.
        assert(3==size(v,1)); % unit_vector should be 3xL.
        assert(1==min(size(x)==size(p)));        
    end
    
    n=eye(3,3)-v*v';    % Normal vector to unit vector.
    d=norm((x-p)'*n);   % Distance from point x to the line.s
end
