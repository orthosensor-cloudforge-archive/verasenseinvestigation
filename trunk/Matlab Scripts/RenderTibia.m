%%
%   Render the tibia model.
%
%   \param [in] geometry array containing the tibiaLength, Radius, ankle_joint_radius, knee_joint_radius, N.
%   \param [in] T Transformation to apply before rendering.
%   \param [in] cutPlaneView 1=hide kneeHandle, 
function [ output_args ] = RenderTibia(T,geometry,cutPlaneView)

persistent ankle_jointX ankle_jointY ankle_jointZ ankleHandle
persistent tibia_linkX  tibia_linkY  tibia_linkZ  tibiaHandle
persistent tibiaCircleHandle  tibiaCircleVertices 
persistent knee_jointX  knee_jointY  knee_jointZ  kneeHandle 

if isempty(tibiaHandle)
    %%
    %   Define the tibia geometry.
    %
    assert(2 <= nargin);
    
    %
    %   Represent tibia as cylinder.
    %
    [tibia_linkX,tibia_linkY,tibia_linkZ]=cylinder(geometry(2),geometry(5));  % Set of points for tibia.
    tibia_linkZ=geometry(1)*tibia_linkZ;                               % Scale the length.
    
    tibiaHandle=surf(tibia_linkX,tibia_linkY,tibia_linkZ);
    hold on
    set(tibiaHandle,'FaceColor',[0,0,0.35],'EdgeColor',[0,0,1]);
    set(tibiaHandle,'FaceAlpha',0.9,'EdgeAlpha',.9);

    %
    %   Show the end of the tibia as a circular surface. (Shows for cuts.)
    %
    rad=[0:(2*pi)/geometry(5):+2*pi]'; % Angle in radians.
    vertices = [[0,0,0];cos(rad), sin(rad), zeros(length(rad),1)] * geometry(2);        
    tibiaCircleVertices = tmultp(t1r1(0,geometry(1),2),vertices')';  % Shift out the z axis and transform with rotations.        
    tibiaCircleFaces = [ ones(length(vertices)-2,1), [2:length(vertices)-1]', [3:length(vertices)]'];

    tibiaCircleHandle = patch('Faces',tibiaCircleFaces,'Vertices',tibiaCircleVertices);
    set(tibiaCircleHandle,'FaceColor','b','EdgeColor',[0,0,1]);
    set(tibiaCircleHandle,'FaceAlpha',0.3,'EdgeAlpha',.3);

    %
    %   Ankle and knee joint represented by spheres.
    %
    [x,y,z]=sphere(geometry(5));          
    %
    %   Determine color of the sphere by quadrant.
    %
    for (i=1:3)
        color(:,:,i)=((sign((x+.001).*(y+.001).*(z+.001)))+1)/2;   % Defines color as a triple.    
    end
    color(color~=0)=.5; % Add some color to the white faces.

    ankle_jointX=geometry(3)*x;
    ankle_jointY=geometry(3)*y;
    ankle_jointZ=geometry(3)*z;
    ankleHandle=surf(x,y,z,color);
    %%
    %   Define the knee joint geometry. (could use two spheres close together).
    %
    [x,y,z]=sphere(geometry(5));          
    knee_jointX=geometry(4)*x;
    knee_jointY=geometry(4)*y;
    knee_jointZ=geometry(4)*z + geometry(1);
    kneeHandle=surf(x,y,z,color);
    
    %
    %   Apply the transform.
    %    
    [x,y,z]=tmultp3D(T,ankle_jointX,ankle_jointY,ankle_jointZ);           set(ankleHandle,'XData',x,'YData',y,'ZData',z); 
    [x,y,z]=tmultp3D(T,tibia_linkX, tibia_linkY, tibia_linkZ);            set(tibiaHandle,'XData',x,'YData',y,'ZData',z); 
    pts=tmultp(T,tibiaCircleVertices');                                   set(tibiaCircleHandle,'XData',pts(1,:),'YData',pts(2,:),'ZData',pts(3,:)); 
    [x,y,z]=tmultp3D(T,knee_jointX, knee_jointY, knee_jointZ);            set(kneeHandle ,'XData',x,'YData',y,'ZData',z); 
    
    %set(tibiaHandle,'FaceLighting','gouraud','AmbientStrength',0.1);
else
    %
    %   Handles already filled in. Just update.
    %
    [x,y,z]=tmultp3D(T,ankle_jointX,ankle_jointY,ankle_jointZ);           set(ankleHandle,'XData',x,'YData',y,'ZData',z); 
    [x,y,z]=tmultp3D(T,tibia_linkX, tibia_linkY, tibia_linkZ);            set(tibiaHandle,'XData',x,'YData',y,'ZData',z); 
    pts=tmultp(T,tibiaCircleVertices');                                   set(tibiaCircleHandle,'XData',pts(1,:),'YData',pts(2,:),'ZData',pts(3,:)); 
    
    if (nargin~=3)
        [x,y,z]=tmultp3D(T,knee_jointX, knee_jointY, knee_jointZ);            set(kneeHandle ,'XData',x,'YData',y,'ZData',z); 
    elseif (~cutPlaneView)
        disp(['dispay knee cutPlaneView',num2str(cutPlaneView)]);
        [x,y,z]=tmultp3D(T,knee_jointX, knee_jointY, knee_jointZ);            set(kneeHandle ,'XData',x,'YData',y,'ZData',z); 
    else
        %
        %   Suppress kneeHandle. (cut plane view)
        %
        if (0)
            inspect(kneeHandle)        
            set(kneeHandle,'Visible','off');    % Doesn't seem to work???
            set(kneeHandle,'AlphaData',0.0);
            set(kneeHandle,'FaceAlpha',0.0);
            set(kneeHandle,'EdgeAlpha',0.0);
        else
            set(kneeHandle ,'XData',[],'YData',[],'ZData',[]); 
        end
%        disp('suppress knee?');
    end
end
output_args=[];
end

