function [accel,gyro,magn,time] = collect_9DOM(port,STREAM_N)
%
%   This script collects the 9DOM (degrees of measurements and timestamps).
%   Reads serial stream from Arduino 9DOF board
%    
disp('Orthosensor 9DOM Collection.');
assert(2==nargin);

%%
%   Preallocate and initialize the return values.
%
accel=zeros(STREAM_N,3);
gyro=zeros(STREAM_N,3);
magn=zeros(STREAM_N,3);
time=zeros(STREAM_N,1);

%%
%   Arduino uses DTR,DTS for reset.
%
s_port=serial(['COM',num2str(port)],'BaudRate', 57600,'DataBits',8,'DataTerminalReady','on','RequestToSend','on');
if (1 == strcmp(s_port.status,'closed'))
    s_port.InputBufferSize=5120;  % Make it big enough so we don't drop chars while searching for header.
    fopen(s_port);
    disp('Waiting for Arduino to boot up and start transmitting.');
    %
    %   Read the initial strings from the Arduino.
    %
    stream_header;
    
    for i=1:STREAM_N
        %
        %   Read and verify the header.
        %        
        header=fread(s_port,1,'uchar');
        if (header == hex2dec('FA'))        
            %
            %   Found the streaming command, check the length.
            %
            byte_count=fread(s_port,1,'uchar');
            if (20 == byte_count)
                %
                %   Read the payload portion of the packet.
                %
                payload=fread(s_port,(byte_count/2),'int16')';
                %%
                %   Main window display.
                %
                switch(0)
                    case 0,
                        % Don't display anything. (normal mode)
                    case 1,
                        %
                        %   Print numeric sensor values.  If the BytesAvailable increases it means the PC plotting can't keep up.
                        %
                        disp(sprintf('%d Accel %d %d %d Gyro %d %d %d Mag %d %d %d Timestamp %d available %d\n',i,payload,s_port.BytesAvailable));
                    case 2,
                        %
                        %   Display the serial backlog. For tuning Fupdate.
                        %
                        disp(sprintf('%d (%d)\n',i,s_port.BytesAvailable));
                    otherwise
                        disp('What is this?');                   
                end
                accel(i,:)=payload(1:3);   % Raw values without any scaling.
                gyro(i,:)= payload(4:6)/14.375;   % Scale to rad/sec.                
                magn(i,:)= payload(7:9);          % Scale to gauss.
                %
                %   Update the time period with the time stamp.
                %
                time(i) = payload(10);
            else
                disp(['Wrong byte count ',num2str(byte_count)]);
                break;   % Exit the loop.
            end
        else
            disp(sprintf('OOS %d value %s %c BAvailable %d.\n',i,dec2hex(header),header,s_port.BytesAvailable));
            break; % Exit the loop since we missed data.
        end
    end % for (number of measurements at each position).
    %%
    %   Close down the serial port.
    %
    fclose(s_port);
    
    
    %%
    %   Plot accel data for debugging.
    %
    figure(888);
    clf;
    plot(accel(:,1),'r');
    hold on, title('Accels');ylabel('raw counts');
    plot(accel(:,2),'g');
    plot(accel(:,3),'b');
    legend(['x','y','z']');
else
    disp('serial port not closed.')
end
