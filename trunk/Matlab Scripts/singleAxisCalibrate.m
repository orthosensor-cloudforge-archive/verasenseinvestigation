    for a=1:3
        r1=[(2*(a-1))*N+1:(2*a-1)*N]; % range for +1 values.
        r2=[(2*a-1)  *N+1:(2*a  )*N];  % range for -1 values.

        gain(a) = (mean(accel(r1,a)) - mean(accel_all(r2,a)))/2;
        offset(a) = (mean(accel(r1,a)) + mean(accel_all(r2,a)))/2;
    end  
    %
    %   Create the accel calibration matrix using single axis gain/offsets corrections.
    %
    accel_calibration3=[diag(1./gain);-offset./gain];
    accel=[accel_all,ones(size(accel_all,1),1)] * accel_calibration3;

    %%
    %   Plot values corrected with three single axis calibrations.
    %
    figure(2)
    clf
    plotColor=['r','g','b'];
    for a=1:3
        r1=[(2*(a-1))*N+1:(2*a-1)*N]; % range for +1 values.
        r2=[(2*a-1)  *N+1:(2*a  )*N];  % range for -1 values.

        plot(accel(r1,1),'r'), hold on
        plot(accel(r1,2),'g')
        plot(accel(r1,3),'b')
        plot(accel(r2,1),'r')
        plot(accel(r2,2),'g')
        plot(accel(r2,3),'b')
    end  
    grid on
    title('Three single axis calibrations.')
    ylabel('[g]');
    xlabel('sample');
    axis([0,N,-1.1,+1.1]);

    figure(3)
    clf
    plot3([-1,+1],[ 0, 0],[ 0, 0],'k'), hold on
    plot3([ 0, 0],[-1,+1],[ 0, 0],'k')
    plot3([ 0, 0],[ 0, 0],[-1,+1],'k')
    for a=1:3
        r1=[(2*(a-1))*N+1:(2*a-1)*N]; % range for +1 values.
        r2=[(2*a-1)  *N+1:(2*a  )*N];  % range for -1 values.

        %[mean(accel_all(r1,:)), 1]
        v=mean(accel(r1,:));
        plot3([0,v(1)],[0,v(2)],[0,v(3)],plotColor(a))
        v=mean(accel(r1,:));
        plot3([0,v(1)],[0,v(2)],[0,v(3)],[plotColor(a),'-'])

        plot3(accel(r1,1),accel(r1,2),accel(r1,3),[plotColor(a),'.']) % Plot clusters of points.
        plot3(accel(r2,1),accel(r2,2),accel(r2,3),[plotColor(a),'.'])
    end  
    grid on
    title('Corrected single axis cal.')
    xlabel('[Gx]');
    ylabel('[Gy]');
    zlabel('[Gz]');
    axis([-1.1,+1.1,-1.1,+1.1,-1.1,+1.1],'equal');
