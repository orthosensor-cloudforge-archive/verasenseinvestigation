%%
%   Matlab script defining model geometry.
%   
%   This script is develops the forward or configuration kinematic equations. (determine position and orientation of knee given
%   the joint variables.)
%   This is useful for testing ideas on how to solve the inverse kinematics problem of solving for joint variables given
%   observations of the knee position and orientation.
%
%   Frame 0 - (Gravity/World) x up relative to gravity. z points to knee joint.
%   Frame 1 - (Knee Frame) z from ankle to knee joint.

%%
%   Describe the model geometry.
%
%   Tibia
%
%   Femur
%       2 joints describe the ball and socket hip joint. (zero length link between)
%       1 link at the ball and socket is zero length to allow the 2DOF joint to be modeled as 2 1DOF joints.
%       1 link models the skeleton link between the hip joints and the IMU.
%       1 joint models the attachment of the IMU to the femur.
%   Unit axis.
global tibia_length tibiaGeometry
%%
%   Dimensions of the model. [meters]
%
tibia_length= 0.45;
tibia_radius= 0.01;
ankle_joint_radius= 0.015;
knee_joint_radius=  0.02;
tibiaGeometry=[tibia_length, tibia_radius, ankle_joint_radius, knee_joint_radius, 15];

femur_link_radius=0.02;
f1_link_length=0.10;
f1_link_radius=0.01;
f2_link_radius=0.01;
f2_link_length=0.45;
femur_length=sqrt(f1_link_length^2+f2_link_length^2);

hip_joint_radius=0.03;

units0=[zeros(3,1), eye(3,3)];

%%
%   Describe Geometry of each Link relative to its own frame.
%


%%
%   Render the geometry in body frame.
%
figure(1);
clf;
clear RenderVeraSense   % Clear local function variables.
clear RenderArc         % Clear local function variables.
clear RenderTibia       % Clear local function variables.
RenderTibia(eye(3,4),tibiaGeometry);

axis('equal')
axis([-0.0,+0.4, -0.1,+0.1, -0.1,tibia_length+femur_length])

title('Geometry in body frame');
xlabel('x_a');
ylabel('y_a');
zlabel('z_a');
campos([2,1.5,-5]);
camup([1 0 0]); % x up.

%%
%   Define the femur geometry in knee frame. (two links and a sphere for hip joint.)
%
%   The femur is modeled as two rigid links at 90 degrees.
%   Each link is represented by a cylinder with a specified radius and length.
%
%%
%   Define the femur geometry in the knee frame.
%
global f1LinkHandle f1_linkX_0 f1_linkY_0 f1_linkZ_0
global f2LinkHandle f2_linkX_0 f2_linkY_0 f2_linkZ_0
global f3LinkHandle j_x_0 j_y_0 j_z_0
global f4LinkHandle hip_ball_jointX_0 hip_ball_jointY_0 hip_ball_jointZ_0

[f1_linkX_0,f1_linkY_0,z]=cylinder(f1_link_radius,15);  % Set of points for hip bone1.
f1_linkZ_0=f1_link_length*z;                            % Scale the length.
T=tmultt(t1r1(0,f2_link_length,2),t1r1(90,0,0)); % Rotate then translate. (first operation goes into second parameter.   
[f1_linkX_0,f1_linkY_0,f1_linkZ_0]=tmultp3D(T,f1_linkX_0,f1_linkY_0,f1_linkZ_0);

[f2_linkX_0,f2_linkY_0,f2_linkZ_0]=cylinder(f2_link_radius,15); % Set of points for hip bone2.
f2_linkZ_0=f2_link_length*f2_linkZ_0;  % Scale length.
%%
%   Draw another sphere to smooth connection of the two link cylinders.
%
j_radius=max(f1_link_radius,f2_link_radius);
[x,y,z]=sphere(15);   % Set of points for sphere representing the joint.
j_x_0=j_radius*x;
j_y_0=j_radius*y;
j_z_0=j_radius*z + f2_link_length; 

%
%   Determine color of the sphere by quadrant.
%
for (i=1:3)
    color(:,:,i)=((sign((x+.001).*(y+.001).*(z+.001)))+1)/2;   % Defines color as a triple.    
end
color(color~=0)=.5; % Add some color to the white faces.


%%
%   Define the hip joint geometry in knee frame.
%
[x,y,z]=sphere(15);   % Set of points for 1cm sphere representing the hip joint.
hip_ball_jointX_0=hip_joint_radius*x;
hip_ball_jointY_0=hip_joint_radius*y - f1_link_length;
hip_ball_jointZ_0=hip_joint_radius*z + f2_link_length;
%
%   Rotate all the femur and hip geometry so hip joint is on the z axis.
%
T=t1r1(-atan(f1_link_length/f2_link_length)*180/pi,0,0);

[f1_linkX_0,f1_linkY_0,f1_linkZ_0]=tmultp3D(T,f1_linkX_0,f1_linkY_0,f1_linkZ_0);
[f2_linkX_0,f2_linkY_0,f2_linkZ_0]=tmultp3D(T,f2_linkX_0,f2_linkY_0,f2_linkZ_0);
[j_x_0     ,j_y_0,     j_z_0]=tmultp3D(T,j_x_0,j_y_0,j_z_0);
[hip_ball_jointX_0,hip_ball_jointY_0,hip_ball_jointZ_0]=tmultp3D(T,hip_ball_jointX_0,hip_ball_jointY_0,hip_ball_jointZ_0);

%%
%   Render VeraSense
%
RenderVeraSense(eye(3,4));  % Display VeraSense.

%%
%   Display femur geometry in body frame. (Only equivalent to accel frame when ankle and hip joints are level.)
%   Don't change original _0 geometery variables.
T=t1r1(0,tibia_length,2);

[x,y,z]=tmultp3D(T,f1_linkX_0,f1_linkY_0,f1_linkZ_0);       f1LinkHandle=mesh(x,y,z);
[x,y,z]=tmultp3D(T,f2_linkX_0,f2_linkY_0,f2_linkZ_0);       f2LinkHandle=mesh(x,y,z);
[x,y,z]=tmultp3D(T,j_x_0     ,j_y_0,     j_z_0);            f3LinkHandle=surf(x,y,z,color);
[x,y,z]=tmultp3D(T,hip_ball_jointX_0,hip_ball_jointY_0,hip_ball_jointZ_0);  f4LinkHandle=surf(x,y,z, color);

RenderFemur(eye(3,4));
%%
%   Move the model into the surgical 45 degree tibia, 0 degree tilt position. (keep ankle as origin, should I have chosen hip as origin???)
%   
tibiaElevationDegrees=45;
Tbody=t1r1(tibiaElevationDegrees,0,1);
RenderTibia(Tbody);
RenderVeraSense(Tbody);
%
%   Rotate the femur down, then shift to body frame, and apply body T.
%
femur_angle=-(180-(90-tibiaElevationDegrees)-acos(tibia_length*sin(tibiaElevationDegrees*pi/180)/femur_length)*180/pi);

T=tmultt(Tbody,tmultt(t1r1(0,tibia_length,2),t1r1(femur_angle,0,1)));
RenderFemur(T);