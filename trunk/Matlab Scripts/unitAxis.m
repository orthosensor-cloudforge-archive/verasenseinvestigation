%%
%   This function will plot the unit axis 
%
%   I should be able to do this with one handle and a vectors, but for now just use 3 handles.
%
%   \param [in] array of points. First point is the origin, second is x axis, y, z.
%   \param [in] scale
%   \param [in] handle
function [ handle ] = unitAxis(pts,scale,handle,lineWidth)

    assert(min([3,4] == size(pts)));
    switch(nargin)
        case 2, 
            handle(1)=quiver3(pts(1,1),pts(2,1),pts(3,1), scale*(pts(1,2)), scale*(pts(2,2)), scale*(pts(3,2)),'r','LineWidth',1);
            handle(2)=quiver3(pts(1,1),pts(2,1),pts(3,1), scale*(pts(1,3)), scale*(pts(2,3)), scale*(pts(3,3)),'g','LineWidth',1);
            handle(3)=quiver3(pts(1,1),pts(2,1),pts(3,1), scale*(pts(1,4)), scale*(pts(2,4)), scale*(pts(3,4)),'b','LineWidth',1);
        case 3,
            set(handle(1),'Xdata',pts(1,1),'YData',pts(2,1),'ZData',pts(3,1),'UData',scale*pts(1,2),'VData',scale*pts(2,2),'WData',scale*pts(3,2),'LineWidth',1.5);
            set(handle(2),'Xdata',pts(1,1),'YData',pts(2,1),'ZData',pts(3,1),'UData',scale*pts(1,3),'VData',scale*pts(2,3),'WData',scale*pts(3,3),'LineWidth',1.5);
            set(handle(3),'Xdata',pts(1,1),'YData',pts(2,1),'ZData',pts(3,1),'UData',scale*pts(1,4),'VData',scale*pts(2,4),'WData',scale*pts(3,4),'LineWidth',1.5);
        otherwise
            assert(0,'wrong number of arguments.');
    end
end

