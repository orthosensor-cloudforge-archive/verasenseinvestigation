%
%   Use files from VeraSense RxTxUtility.
%
p=1;
load(['RxTxData_Yellow/Position',num2str(p),'.txt'])
 %%

accel=eval(['Position',num2str(p),'(:,1:3)']);
%
%   Plot raw counts.
%
figure(1)
clf
plot(accel(:,1),'r'); hold on
plot(accel(:,2),'g'); 
plot(accel(:,3),'b'); 
title(['Raw Counts Position ']);
ylabel('AtoD');
xlabel('sample')
ticks=0:8192:65536;
set(gca,'YTick',ticks)
set(gca,'YTickLabel',num2str(ticks'))
axis([0,size(accel,1),0,2^16]);
grid on


%
%   Look at noise. (throw out the dc.)
%
figure(98)
clf
accel_lowpass=accel-ones(size(accel,1),1)*mean(accel)

plot(accel_lowpass(:,1),'r'); hold on
plot(accel_lowpass(:,2),'g'); 
plot(accel_lowpass(:,3),'b'); 
title(['Noise Counts']);
ylabel('AtoD');
xlabel('sample')
axis([0,size(accel_lowpass,1),-1000,500]);
grid on

%
%   Look at spectrum of noise.
%
figure(99)
x=accel_lowpass(:,1);
N = length(x);
Fs=1;
xdft = fft(x);
xdft = xdft(1:N/2+1);
psdx = (1/(Fs*N)).*abs(xdft).^2;
psdx(2:end-1) = 2*psdx(2:end-1);
freq = 0:Fs/length(x):Fs/2;
plot(freq,10*log10(psdx)); grid on;
title('Periodogram Using FFT');
xlabel('Frequency (Hz)'); ylabel('Power/Frequency (dB/Hz)');
