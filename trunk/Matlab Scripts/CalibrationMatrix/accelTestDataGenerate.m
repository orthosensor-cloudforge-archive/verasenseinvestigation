%
%   Generate test data for LabView vs Matlab comparisons.
%

N=10;    % Number of measurements at each position.

AtoD1G=24080;   % Scaling from Hardware.
%
%
%
positions=[
    [+1,0,0];
    [-1,0,0];
    [ 0,+1,0];
    [ 0,-1,0];
    [ 0,0,+1];
    [ 0,0,-1];];

Y=[];
for (p=1:size(positions,1 ))
    %
    %   Known positions.
    %
    Y=[Y; ones(N,1)*positions(p,:)];
end

%
%   Scale to AtoD counts with 0.0G at 32768
%
Y=Y*AtoD1G + 2^15;
round(Y) % Display as int.


if (1)
    X=Y;
    %
    %   Add a little random noise to measurements.
    %
    X=X+(rand(6*N,3)-0.5)*100;
    %
    %   Add a little DC offset to each axis.
    %
    X(:,1)=X(:,1)+100;
    X(:,2)=X(:,2)- 50;
    X(:,3)=X(:,3)+150;
else
X=Y;
end

lscov([X,ones(6*N,1)],Y)



