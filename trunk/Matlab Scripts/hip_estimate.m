%
%   Calculate the measurement accuracy for inclination and position resolution.
%
swing_angle=45;     % Degrees of movement in one plane.
femur_length=0.45;   % Distance between hip socket center of rotation and IMU.


half_angle=(swing_angle/2)*pi/180;
d1=femur_length*sin(half_angle);
adjacent=femur_length*cos(half_angle);

%%
%   Plot the trajectory of the knee joint (in 2D).
%
figure(10);
plot([0,femur_length],[0,0],'b')
hold on
axis equal
plot([adjacent,adjacent],[-d1,d1],'b');

%%
%   Plot the arc.
%
arc_angles=[-half_angle:1*(pi/180):+half_angle];
plot(femur_length*cos(arc_angles),femur_length*sin(arc_angles))
plot([0,+adjacent],[0,+d1])
plot([0,+adjacent],[0,-d1])

%%
%   Overlay IMU resolution limits.  Assume perfect position (d1 height) information.
%
IMU_resolution=+1.0;   % Degrees of resolution.
d2=femur_length*cos(half_angle+IMU_resolution*pi/180);
disp(sprintf('IMU orientation resolution %+3.2f [degree] off by %3.2f [mm].',IMU_resolution,(adjacent-d2)*10^3))
IMU_resolution=-IMU_resolution;   % Degrees of resolution.
d2=femur_length*cos(half_angle+IMU_resolution*pi/180);
disp(sprintf('IMU orientation resolution %+3.2f [degree] off by %3.2f [mm].',IMU_resolution,(adjacent-d2)*10^3))

IMU_resolution=+0.2;   % Degrees of resolution.
d2=femur_length*cos(half_angle+IMU_resolution*pi/180);
disp(sprintf('IMU orientation resolution %+3.2f [degree] off by %3.2f [mm].',IMU_resolution,(adjacent-d2)*10^3))
IMU_resolution=-IMU_resolution;   % Degrees of resolution.
d2=femur_length*cos(half_angle+IMU_resolution*pi/180);
disp(sprintf('IMU orientation resolution %+3.2f [degree] off by %3.2f [mm].',IMU_resolution,(adjacent-d2)*10^3))

IMU_resolution=+0.1;   % Degrees of resolution.
d2=femur_length*cos(half_angle+IMU_resolution*pi/180);
disp(sprintf('IMU orientation resolution %+3.2f [degree] off by %3.2f [mm].',IMU_resolution,(adjacent-d2)*10^3))
IMU_resolution=-IMU_resolution;   % Degrees of resolution.
d2=femur_length*cos(half_angle+IMU_resolution*pi/180);
disp(sprintf('IMU orientation resolution %+3.2f [degree] off by %3.2f [mm].',IMU_resolution,(adjacent-d2)*10^3))
%%
%   Assume the IMU is perfect, but position is off.
%
distance_off=d1*.01;    % 1 percent off.
err=distance_off*tan(half_angle);
disp(sprintf('IMU position resolution %+3.2f [mm] off by %3.2f [mm].',distance_off*10^3,err*10^3))

distance_off=d1*.01;    % 1 percent off.
err=distance_off*tan(half_angle);
disp(sprintf('IMU position resolution %+3.2f [mm] off by %3.2f [mm].',distance_off*10^3,err*10^3))





