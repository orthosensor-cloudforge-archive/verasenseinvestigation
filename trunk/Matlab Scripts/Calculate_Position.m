%
%   Position.
%
%   Postion can be optained by double integration of acceleration.
%   We use the orientation information to translate IMU accleration to frame 0 coordinates.
%o
%   1) Detect movement using change in signals. (zero bias drift while still)
%   2) Translate acceleration using orientation.
%       Subtract of gravity.
%   3) Integrate to velocity and again to position.
%
function [p,v]=Calculate_Position(acceleration, ypr, t_s, v_0, p_0)

    persistent acceleration_old;    % Last value used for trapezoidal approximation for integration.
    persistent velocity_old;        % Last value used for trapezoidal approximation for integration.
    persistent velocity;            % Integrated velocity.
    persistent position;            % Integrated position.
    persistent i;                   % Counter for how many steps of integration have been performed.
    
    assert(3==size(acceleration,1));
    assert(3==size(ypr,1));
    
    if isempty(velocity) 
        switch(nargin)
            case 3,
                velocity=[0.0;0.0;0.0];  % Initial velocity (frame_0).
                position=[0.0;0.0;0.0];  % Initial position (frame_0).
            case 4, % Initial velocity specified.
                velocity=v_0;
            case 5, % Initial position specified.
                velocity=v_0;
                position=p_0;
            otherwise
                disp(['Calculate_Position(nargin=',num2str(nargin)]);
        end
        i=1;                     % Index.
        
        acceleration_old=0;
        velocity_old=velocity;
    end
    %
    %   Use orientation to rotate acceleration back to stationary frame.
    %   ypr contains Yaw, Pitch, Roll.  Apply in order of roll, pitch, roll to get to fixed frame coordinates.
    %
    rotation_x=t1r1( ypr(3,1),0,0);     % Roll Rotation about x. 
    rotation_y=t1r1(-ypr(2,1),0,1);     % Pitch rotation about y. Notice sign reversal to get relative to IMU rather than aircraft convention.
    rotation_z=t1r1(-ypr(1,1),0,2);     % Yaw rotation about z.  Notice sign reversal to get relative to IMU rather than aircraft convention.
    %
    %   This matrix transforms the IMU coordinates to stationary XYZ frame. Fixed XYZ frame.
    %
    R=rotation_z(:,1:3)*rotation_y(:,1:3)*rotation_x(:,1:3);    % Only need the rotation portion of the translation matrix.
    
    acceleration_0=R*acceleration;  % Acceleration relative to stationary frame.
    
    if (1)
        acceleration_0=acceleration_0 - [0;0;9.81];    % Subtract off gravity.
    else
        disp('Calculate_Position is ignoring gravity.');
    end
    
    %
    %   Integrate acceleration to velocity and velocity to position.
    %
    if (0)
        %
        %   Rectangular Rule approximation to integration.
        %
        velocity=velocity + (acceleration_0*t_s);
        position=position + (velocity      *t_s);
    else
        %
        %   Trapezoidal Rule approximation to integration.
        %
        velocity=velocity + (acceleration_0+acceleration_old)/2*t_s;
        position=position + (velocity      +velocity_old    )/2*t_s;
        
        acceleration_old=acceleration_0;
        velocity_old=velocity;
    end
    %
    %   Return position and velocity.
    %
    p=position;
    v=velocity;
    
%    format long
%    [acceleration_0, velocity, position]'
%    format short
    i=i+1;
end % Calculate_Position.


