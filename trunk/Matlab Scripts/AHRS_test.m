%
%
%
addpath([pwd,'\AHRS'],'-end');  % AHRS function scripts are in AHRS subdirectory.

q=[1;0;0;0];
FS=100; % Sample frequency. [Hz]

halfT=1/200;    % Initial guess at time period.

for (i=1:10000)
    %%
    % Measure the time period.  This should be replaced with a time stamp from the embedded controller.
    %
    t0=tic;    
    %%
    %   New sensor data.
    %
    accel=[9.8; 0.0; 0.0 ];
    gyro= [0.0; 0.0; 0.0 ];
    mag=  [1.0; 0.0; 0.0 ];
    %%
    %   Correct with sensor calibration.
    %
    
    
    
    %%
    %   Update AHRS algorithm.
    %
    q=AHRSUpdate(accel,gyro,mag,1/FS/2,q);
    angles=getEuler(q);
    [angles']
    %getYawPitchRoll();
    
    %
    %   Update the model display.
    %
    
    
    %
    %   Update the time period. (Replace this with a time stamp.)
    %
    halfT = toc(t0) / 2;    
end
    