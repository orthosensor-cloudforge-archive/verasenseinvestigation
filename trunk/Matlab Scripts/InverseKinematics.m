%%
%   Solve inverse kinematics. Find the VeraSense orientation given the accels along the surgical path.
%

%%
%   Try brute force search for VT closest to desired path.
%
%%
%   Solve for rotation matrix
%

%
%   Define range to search over.
%
rotX=+1.0;      % Use same sign as Gordon. The search range is centered on this value.
rotY=+0.0;      % Use negative sign as Gordon.
rotZ=+1.0;      % Use negative sign as Gordon.
%    searchRangeX=[VeraSenseDegrees(1)-1.0: 0.05: VeraSenseDegrees(1)+1.0];
%    searchRangeY=[VeraSenseDegrees(2)-1.0: 0.05: VeraSenseDegrees(2)+1.0];
%    searchRangeZ=[VeraSenseDegrees(3)-1.0: 0.05: VeraSenseDegrees(3)+1.0];
searchRangeX=[rotX- 1.0   : 0.05: rotX + 1.0];
searchRangeY=[rotY- 1.0   : 0.05: rotY + 1.0];
searchRangeZ=[rotZ- 1.0   : 0.05: rotZ + 1.0];
%
%   Use settings at the top of SurgicalMovement to calculate accelAll measurements.
%
if (1)
    SurgicalMovement
    %
    %   A/D will loose some resolution. Model that here.
    %
    if (0)
        disp('Limited resolution to 14 bits.')
        accelAll=round(accelAll*2^14)/2^14;
    else
        disp('Full resolution.')
    end

    %%
    %   This assumes we know tiltDegrees for each measurement.
    %
    b=tmultp(t1r1(-tibiaElevationDegrees,0,1),[cos(-tiltDegrees'*pi/180), sin(-tiltDegrees'*pi/180), zeros(size(tiltDegrees'))]')';
    filename='Simulated';        
    figNumber=87;   % Plot figure number for the first of three 3d plots of the error surface.
else
    %%
    %
    %
    if (0)
        accelAll=jjt;
        tiltDegrees=jjtTilt;
        plotAccels;
    end
%    filename='.\GordonData\file2.csv';
%    filename='.\GordonData\data(+)_-4.00_0.00.csv';
%    filename='.\GordonData\data(+)_4.00_3.89_0.00.csv';
    filename='.\GordonData\data(-)_1.00_0.00_-1.00.csv';    tibiaElevationDegrees=45;     figNumber= 87;
%    filename='.\GordonData\data(-)_1.00_-27.00_-1.00.csv';  tibiaElevationDegrees=18;     figNumber=100;
    accelAll = load(filename);
    %
    %   A/D will loose some resolution. Model that here. (Before tilt estimate.)
    %
    if (0)
        disp('Limited resolution to 14 bits.')
        accelAll=round(accelAll*2^14)/2^14;
    else
        disp('Full resolution.')
    end
    accelOriginal=[];
    tiltDegrees=[];
    b=[];
    %
    %   Transform back to original as a quick check.
    %
    if (1)
        Tzyx=tmultt(t1r1(-rotZ,0,2),tmultt(t1r1(-rotY,0,1),t1r1(-rotX,0,0)));  % misadjustment.
        Vinv=[inv(Tzyx(:,1:3)),[0;0;0]];
        z = tmultp(Vinv,accelAll')';  % This is the measurement corrected for what we are guessing the VeraSense misalignment is.
        %
        %   Use Vinv and measurement to estimate the anatomical tilt.
        %
        tiltEst=atan2(-z(:,2),sqrt(z(:,1).^2 + z(:,3).^2))*180/pi;
        figure(2);

        title('Inverse Kinematics Est Tilt');
        plot(tiltEst,z(:,1),'r+'); hold on
        plot(tiltEst,z(:,2),'g+');
        plot(tiltEst,z(:,3),'b+');
        xlabel('estimated tilt[degrees]');
        ylabel('accel [G]');
        grid on
        axis('tight');
    end
end

if (1)
    %%
    %   Brute force search by assuming an orientation and calculating the difference from ideal path.
    %
    errSurface=zeros(length(searchRangeX),length(searchRangeY),length(searchRangeZ));
    tic
    for r=1:length(searchRangeX)
        Tx=t1r1(-searchRangeX(r),0,0);

        for c=1:length(searchRangeY)
            Tyx=tmultt(t1r1(-searchRangeY(c),0,1),Tx);

            for p=1:length(searchRangeZ)
                Tzyx=tmultt(t1r1(-searchRangeZ(p),0,2),Tyx);  % VeraSense assumed misadjustment.
                Vinv=[inv(Tzyx(:,1:3)),[0;0;0]];

                z = tmultp(Vinv,accelAll')';  % This is the measurement corrected for what we are guessing the VeraSense misalignment is.
                if (1)
                    %
                    %   Use Vinv and measurement to estimate the anatomical tilt. This does not use apriori knowledge of tilt.
                    %
                    tiltEst=atan2(-z(:,2),sqrt(z(:,1).^2 + z(:,3).^2))*180/pi;
                    b=tmultp(t1r1(-tibiaElevationDegrees,0,1),[cos(-tiltEst*pi/180), sin(-tiltEst*pi/180), zeros(size(tiltEst))]')';  % Ideal movement for this tibiaElevationDegrees and tiltEst.
                end
                e = z-b;

                errSurface(r,c,p)=sum(sum(e.*e))/numel(e);        
            end   
        end
    end
    toc
end
%%
%   Minimum of errSurface is best estimate of VeraSense angles.
%
%errSurface=(errSurface45+errSurface18)/2;
mErrSurface=min(min(min(errSurface)));
index=find(errSurface <= mErrSurface);
[i,j,k]=ind2sub(size(errSurface),index);
%[searchRangeX(i),searchRangeY(j),searchRangeZ(k),mErrSurface]


%
%   Display the estimated tilt range.
%

Tx=         t1r1(-searchRangeX(i),0,0);
Tyx= tmultt(t1r1(-searchRangeY(j),0,1),Tx);
Tzyx=tmultt(t1r1(-searchRangeZ(k),0,2),Tyx);  % VeraSense misadjustment.

Vinv=[inv(Tzyx(:,1:3)),[0;0;0]];

z = tmultp(Vinv,accelAll')';  % This is the measurement corrected for what we are guessing the VeraSense misalignment is.
tiltEst=atan2(-z(:,2),sqrt(z(:,1).^2 + z(:,3).^2))*180/pi;

disp(sprintf('%s \t[%+5.2f %+5.2f %+5.2f] err=%e tilt range %+5.2f to %+5.2f.',filename,searchRangeX(i),searchRangeY(j),searchRangeZ(k),mErrSurface,tiltEst(1),tiltEst(length(tiltEst))));

%%
%   Plot the error as a function of any two angles.
%
figure(figNumber), clf
errSurf=reshape(errSurface(:,:,k),length(searchRangeX),length(searchRangeY))';  % Notice, transpose is required to keep dimensions correct.
surf(searchRangeX,searchRangeY,errSurf)
hold on
plot3(searchRangeX(i),searchRangeY(j),mErrSurface,'m*')
plot3(searchRangeX(i),searchRangeY(j),mErrSurface,'m+')
plot3([searchRangeX(i),searchRangeX(i)],[min(searchRangeY),max(searchRangeY)],[0,0],'m')
plot3([min(searchRangeX),max(searchRangeX)],[searchRangeY(j),searchRangeY(j)],[0,0],'m')
title(['Brute Force Search X=',num2str(searchRangeX(i)),' Y= ',num2str(searchRangeY(j)),' Z= ',num2str(searchRangeZ(k))]);
xlabel('RotX'); ylabel('RotY'); zlabel('err');

figure(figNumber+1), clf
errSurf=reshape(errSurface(:,j,:),length(searchRangeX),length(searchRangeZ))';
surf(searchRangeX,searchRangeZ,errSurf)
hold on
plot3(searchRangeX(i),searchRangeZ(k),mErrSurface,'m*')
plot3(searchRangeX(i),searchRangeZ(k),mErrSurface,'m+')
plot3([searchRangeX(i),searchRangeX(i)],[min(searchRangeZ),max(searchRangeZ)],[0,0],'m')
plot3([min(searchRangeX),max(searchRangeX)],[searchRangeZ(k),searchRangeZ(k)],[0,0],'m')
title(['Brute Force Search X=',num2str(searchRangeX(i)),' Y= ',num2str(searchRangeY(j)),' Z= ',num2str(searchRangeZ(k))]);
xlabel('RotX'); ylabel('RotZ'); zlabel('err');

figure(figNumber+2), clf
errSurf=reshape(errSurface(i,:,:),length(searchRangeY),length(searchRangeZ))';
surf(searchRangeY,searchRangeZ,errSurf)
hold on
plot3(searchRangeY(j),searchRangeZ(k),mErrSurface,'m*')
plot3(searchRangeY(j),searchRangeZ(k),mErrSurface,'m+')
plot3([searchRangeY(j),searchRangeY(j)],[min(searchRangeZ),max(searchRangeZ)],[0,0],'m')
plot3([min(searchRangeY),max(searchRangeY)],[searchRangeZ(k),searchRangeZ(k)],[0,0],'m')
title(['Brute Force Search X=',num2str(searchRangeX(i)),' Y= ',num2str(searchRangeY(j)),' Z= ',num2str(searchRangeZ(k))]);
xlabel('RotY'); ylabel('RotZ'); zlabel('err');

%%
%   Display isosurface.
%
if (0)
    implicitErrorSurface
end

if (0)
    %%
    %   Display cut plane.
    %
    tiltDegrees=0;
    T=tmultt(t1r1(hipElevationDegrees,0,1),tmultt(t1r1(tiltDegrees,0,2),t1r1(tibiaElevationDegrees-hipElevationDegrees,0,1)));    
    figure(1)
    RenderTibia(T,tibiaGeometry,1);    % Suppress the knee object display.
    RenderTibia(T,tibiaGeometry,1);    % Suppress the knee object display.
    RenderTibia(T,tibiaGeometry,1);    % Suppress the knee object display.
    RenderTibia(T,tibiaGeometry,1);    % Suppress the knee object display.
    unitAxis(tmultp(tmultt(T,t1r1(0,tibia_length,2)),[[0;0;0], eye(3,3)]),0.2);  % Original position without any VeraSense misadjustment.
    %
    %   Don't display femur or tilt arc anymore.
    %
    RenderFemur();
    RenderArc();


    [searchRangeX(i),searchRangeY(j),searchRangeZ(k)]
    campos([1,1,2]);
    %
    %   Zoom in.
    %
    axis([.25,.4,-.1,.1,.2,.4]')
    for (g=1:1)
        %
        %   Use calculated solution for the VeraSense display.
        %
        Tx=t1r1(-searchRangeX(i),0,0);
        Tyx=tmultt(t1r1(-searchRangeY(j),0,1),Tx);
        Tzyx=tmultt(t1r1(-searchRangeZ(k),0,2),Tyx);  % VeraSense misadjustment.

        VTest=tmultp(t1r1(0,tibia_length,2),Tzyx);
        Vinv=[inv(Tzyx(:,1:3)),[0;0;0]];
        RenderVeraSense(tmultt(T,VTest));
        pause(1)
        %
        %   Plot the expected solution for the VeraSense display.
        %
        Tx=t1r1(-searchRangeX(i),0,0);
        Tyx=tmultt(t1r1(-searchRangeY(j),0,1),Tx);
        Tzyx=tmultt(t1r1(-searchRangeZ(k),0,2),Tyx);  % VeraSense misadjustment.

        VTest=tmultp(t1r1(0,tibia_length,2),Tzyx);
        Vinv=[inv(Tzyx(:,1:3)),[0;0;0]];
        RenderVeraSense(tmultt(T,VTest));

        pause(1)
    end
end