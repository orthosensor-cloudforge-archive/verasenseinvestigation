%
%   Plot an isosurface to illustrate the data which is dependent on 3 rotations.
%
figure(44)
clf, 
title('Iso Err Surface')
hold on

[X,Y,Z]=ndgrid(searchRangeX,searchRangeY,searchRangeZ);

%
%   Pick c so the isosurface just touches the plot boundary. If I rotated the axis to ellipse this would occur at axis boundary.
%
c=min([errSurface(1,                    j,                      k),...
      errSurface(length(searchRangeX),  j,                      k),...
      errSurface(i,                     1,                      k),...
      errSurface(i,                     length(searchRangeY),   k),...
      errSurface(i,                     j,                      1),...
      errSurface(i,                     j,                      length(searchRangeY))]);

aa=isosurface(X,Y,Z,errSurface,c);
p=patch(aa);
set(p,'FaceColor','red','EdgeColor','none');
view(3);  axis([searchRangeX(1),searchRangeX(length(searchRangeX)),searchRangeY(1),searchRangeY(length(searchRangeY)),searchRangeZ(1),searchRangeZ(length(searchRangeZ))])
grid on; lighting gouraud;

tval = 1; %transparency value between 0 and 1
alpha(tval);
light('Position',[-1 -1 1],'Style','infinite');
camlight headlight; 
xlabel('X rot')
ylabel('Y rot')
zlabel('Z rot')


plot3([min(searchRangeX),max(searchRangeX)],    [searchRangeY(j),searchRangeY(j)],          [searchRangeZ(k),searchRangeZ(k)],'m')
plot3([searchRangeX(i),searchRangeX(i)],        [min(searchRangeY),max(searchRangeY)],      [searchRangeZ(k),searchRangeZ(k)],'m')
plot3([searchRangeX(i),searchRangeX(i)],        [searchRangeY(j),searchRangeY(j)],          [min(searchRangeZ),max(searchRangeZ)],'m')
plot3(searchRangeX(i),searchRangeY(j),searchRangeZ(k),'k+') % Optimal value.

if (0)
    indices=find(errSurface <= c);
    [x,y,z]=ind2sub(size(errSurface),indices);
    plot3(searchRangeX(x),searchRangeY(y),searchRangeZ(z),'m+')
end